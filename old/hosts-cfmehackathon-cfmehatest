##########
# layer1 #
##########
[layer1]
layer1host              ansible_host="{{ layer1_server }}"
kvmhost                 ansible_host="{{ kvm_server }}"    access_ip="{{ gateway }}"
lxdhost                 ansible_host="{{ lxd_server }}"    access_ip="{{ gateway }}"

# naming convention: lxc-<hostname>
# needed params are: ansible_connection, ansible_host, initip, nic1 and flavor (only the mirror needs also product=mirror)
[lxc-containers]
lxc-cfmehackathon-mirror     ansible_connection=lxd ansible_host="{{ lxd_server }}":lxc-cfmehackathon-mirror    initip="{{ mirror.ip }}"    nic1="{{ mirror.nic1 }}"  flavor="{{ mirror.flavor }}"   product=mirror

# above containers which need to run in privileged context are named here
[extrapriv-containers]

# kvm-containers are (not yet used) lxc containers holding kvm hosts as a way to isolate from the host
[kvm-containers]

# naming convention: kvm-<hostname>
# needed params are: ansible_host, initip, nic1, flavor, image, nodenum (used to produce the vbmc port)
[kvm-vms]
#kvm-cfmehackathon-mirror        ansible_host="{{ kvm_server }}" initip="{{ mirror.ip }}"        nic1="{{ mirror.nic1 }}"       flavor="{{ mirror.flavor }}"       image=CentOS-7-x86_64-GenericCloud.qcow2        nodenum=10
#kvm-cfmehackathon-manageiq-db1  ansible_host="{{ kvm_server }}" initip="{{ manageiq_db1.ip }}"  nic1="{{ manageiq_db.nic1 }}"  flavor="{{ manageiq_db.flavor }}"  image=manageiq-openstack-gaprindashvili-3.qcow2 nodenum=50
#kvm-cfmehackathon-manageiq-db2  ansible_host="{{ kvm_server }}" initip="{{ manageiq_db2.ip }}"  nic1="{{ manageiq_db.nic1 }}"  flavor="{{ manageiq_db.flavor }}"  image=manageiq-openstack-gaprindashvili-3.qcow2 nodenum=51
#kvm-cfmehackathon-manageiq-wrk1 ansible_host="{{ kvm_server }}" initip="{{ manageiq_wrk1.ip }}" nic1="{{ manageiq_wrk.nic1 }}" flavor="{{ manageiq_wrk.flavor }}" image=manageiq-openstack-gaprindashvili-3.qcow2 nodenum=55
#kvm-cfmehackathon-manageiq-wrk2 ansible_host="{{ kvm_server }}" initip="{{ manageiq_wrk2.ip }}" nic1="{{ manageiq_wrk.nic1 }}" flavor="{{ manageiq_wrk.flavor }}" image=manageiq-openstack-gaprindashvili-3.qcow2 nodenum=56
kvm-cfmehackathon-cfme-db1  ansible_host="{{ kvm_server }}" initip="{{ manageiq_db1.ip }}"  nic1="{{ manageiq_db.nic1 }}"  flavor="{{ manageiq_db.flavor }}"  image=cfme-rhevm-5.9.2.4-1.x86_64.qcow2 nodenum=50
kvm-cfmehackathon-cfme-db2  ansible_host="{{ kvm_server }}" initip="{{ manageiq_db2.ip }}"  nic1="{{ manageiq_db.nic1 }}"  flavor="{{ manageiq_db.flavor }}"  image=cfme-rhevm-5.9.2.4-1.x86_64.qcow2 nodenum=51
kvm-cfmehackathon-cfme-wrk1 ansible_host="{{ kvm_server }}" initip="{{ manageiq_wrk1.ip }}" nic1="{{ manageiq_wrk.nic1 }}" flavor="{{ manageiq_wrk.flavor }}" image=cfme-rhevm-5.9.2.4-1.x86_64.qcow2 nodenum=55
kvm-cfmehackathon-cfme-wrk2 ansible_host="{{ kvm_server }}" initip="{{ manageiq_wrk2.ip }}" nic1="{{ manageiq_wrk.nic1 }}" flavor="{{ manageiq_wrk.flavor }}" image=cfme-rhevm-5.9.2.4-1.x86_64.qcow2 nodenum=56

# Baremetal machines (which can be utilized by tripleo/RHOSP/ovirt/RHV) are defined here.
# Needed params are: cpu, ram, mac, ipmi_ip, ipmi_port, ipmi_user, ipmi_pass.
# For kvm-vms this is set automatically during creation.
[baremetal]

##########
# layer2 #
##########
# needed params are: ansible_host
[layer2]
cfmehackathon-mirror        ansible_host="{{ mirror.ip }}"
cfmehackathon-cfme-db1  ansible_host="{{ manageiq_db1.ip }}"
cfmehackathon-cfme-db2  ansible_host="{{ manageiq_db2.ip }}"
cfmehackathon-cfme-wrk1 ansible_host="{{ manageiq_wrk1.ip }}"
cfmehackathon-cfme-wrk2 ansible_host="{{ manageiq_wrk2.ip }}"

#############
# Functions #
#############
[mirror]
cfmehackathon-mirror

[nfs]

[installer]

[packstack-installer]

[director]

[controllervip]

[controllers]

[netnode]

[computes]

[manageiq-primdb]
#cfmehackathon-manageiq-db1 dbnum=1

[manageiq-replica]
#cfmehackathon-manageiq-db2 dbnum=2

[manageiq-engine]
#cfmehackathon-manageiq-wrk1
#cfmehackathon-manageiq-wrk2

[cfme-primdb]
cfmehackathon-cfme-db1 dbnum=1

[cfme-replica]
cfmehackathon-cfme-db2 dbnum=2

[cfme-engine]
cfmehackathon-cfme-wrk1
cfmehackathon-cfme-wrk2

# for names we need and that are acutally other hosts
[aliases]

############
# products #
############
[packstack]

[tripleo]

[ovirt]

[manageiq]

[cfme]
cfmehackathon-cfme-db1
cfmehackathon-cfme-db2
cfmehackathon-cfme-wrk1
cfmehackathon-cfme-wrk2

#################
# distributions #
#################
[rhel7]

[fedora]
layer1host
kvmhost
lxdhost

[centos7]
lxc-cfmehackathon-mirror
kvm-cfmehackathon-mirror
#kvm-cfmehackathon-manageiq-db1
#kvm-cfmehackathon-manageiq-db2
#kvm-cfmehackathon-manageiq-wrk1
#kvm-cfmehackathon-manageiq-wrk2
cfmehackathon-mirror
#cfmehackathon-manageiq-db1
#cfmehackathon-manageiq-db2
#cfmehackathon-manageiq-wrk1
#cfmehackathon-manageiq-wrk2

[rhel7]
kvm-cfmehackathon-cfme-db1
kvm-cfmehackathon-cfme-db2
kvm-cfmehackathon-cfme-wrk1
kvm-cfmehackathon-cfme-wrk2
cfmehackathon-cfme-db1
cfmehackathon-cfme-db2
cfmehackathon-cfme-wrk1
cfmehackathon-cfme-wrk2
