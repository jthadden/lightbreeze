----

# LightBreeze #

----

A lightweight infrastructure installation and testing environment for
OpenSource projects and their related Red Hat products.

## Disclaimer

This git repository only contains personal content, views and opinions
and is not affiliated with Red Hat in any way. Furthermore this is
totally unsupported work. It is not meant to be working or solve anything
you are facing or think or might have as a problem in your life.

**Attention!** This project is not for deploying production environments.

## Documentation ##

### Workshop on OpenStack Summit Vancouver 2018 ###

see Joachim_von_Thadden-Containerize_your_life.pdf

### Create a RHEL LXC image ###

see create_RHEL_image.txt

## Use Cases ##

### UC: Test Octavia Load Balancer (LBaaS)###

See also: https://docs.openstack.org/octavia/latest/user/guides/basic-cookbook.html

**1) prep 2 cirros hosts for http simulation**

- in the GUI create 2 cirros hosts and note the IPs for later
- in the GUI add Floating IPs to them
- ssh into the cirros hosts
- become root: `sudo -i`
- start a minimal http-replying process using `nc`:

` while true; do echo -e "HTTP/1.1 200 OK\nContent-Length: 200\n\n $(ifconfig eth0)" | nc -l -p 80; done`

**2) create a simple http LBaaS**

-  add the internal cirros IPs from above into the vars and execute:

```
CIRROS1IP=10.0.0.5
CIRROS2IP=10.0.0.6
openstack loadbalancer create --name lb1 --vip-subnet-id test_subnet
# wait till ACTIVE:
while true; do openstack loadbalancer list|grep PENDING_CREATE || break ; done
openstack loadbalancer listener create --name listener1 --protocol HTTP --protocol-port 80 lb1
openstack loadbalancer pool create --name pool1 --lb-algorithm ROUND_ROBIN --listener listener1 --protocol HTTP
openstack loadbalancer healthmonitor create --delay 5 --max-retries 4 --timeout 10 --type HTTP --url-path /healthcheck pool1
openstack loadbalancer member create --subnet-id test_subnet --address $CIRROS1IP --protocol-port 80 pool1
openstack loadbalancer member create --subnet-id test_subnet --address $CIRROS2IP --protocol-port 80 pool1
neutron security-group-rule-create --protocol tcp --port-range-min 80 --port-range-max 80 default
```

- in the GUI add a Floating IP to the LB

**3) verify**

- look at the 2 cirros command lines for the *"GET /healthcheck HTTP/1.0"* coming in regularily when we do the following:

```
FLOATIP=192.168.70.30
openstack loadbalancer show lb1
openstack loadbalancer listener show listener1
openstack loadbalancer pool show pool1
openstack floating ip list
curl -v http://$FLOATIP && echo
```

**4) tear down**
openstack loadbalancer delete lb1 --cascade

