# LightBreeze

A lightweight infrastructure installation and testing environment for
Red Hat products and their related OpenSource projects.

## Disclaimer

This git repository only contains personal content, views and opinions
and is not affiliated with Red Hat in any way. Furthermore this is
totally unsupported work. It is not meant to be working or solve anything
you are facing or think or might have as a problem in your life.

**Attention!** This project is not intended for deploying production
environments.

## About ##

I am a Specialist Solution Architect for OpenStack at Red Hat. In this
role I am facing (not only) the following challenges:

- I have to deploy PoC environments, that are normally fairly complex and not some out-of-the-box scenario
- I have to integrate with many different external systems, like authentication (IPA, AD), logging, monitoring, and so on
- I have to add and configure many different OpenStack modules
- I have to deal with very different network setups
- I very often have no external access on those machines, means no internet to download anything - not to speak of remote access to the customers environment via ssh
- I often have to use a customer provided, weird hardened, Windows workstation to do my job via multiple hops over remote access software using putty - meaning that probably half of the keystrokes that are hardcoded in brain will not work properly which slows me down tremendously
- many customers also want to see other products of Red Hat integrated here, too, like CloudForms, OpenShift, and so on
- everything should follow an "Infrastructure as Code" paradigm, so everything has to be automated in a reproducible way that can be checked in into a revision control system like git

To make things happen in a smooth way and to save time at the customer
site, I want to prepare and test those environments thoroughly in
advance. And I am running on very limited resources, meaning I only have
a server with 12 cores (24 threads) and 144 GB of RAM to test that all.

Knowing that, it is obvious that I need

- a system to simulate the reality as close as possible: Bare Metal machines (HDDs, IMPI, NICs to the MAC), networks, and so on
- a system that can also deploy all of that at the customers site preferably without any changes to the plays
- some type of mirror to have all repos and images at hand and transferable/portable to the customer site as well
- all those external systems or some simulation of that: logging, monitoring, authorization, and so on
- something to save precious RAM and Disk space
- a clean automation engine (which happens to be ansible)


## What you gain ##

- complete workflow to
   - prepare your host machine
   - create all LXC and KVM machines
   - bootstrap and install all machines
- flexibility to use all virtualized via KVM, LXC and/or Bare Metal
- the following products so far:
   - RHEL, CentOS, Fedora as base operating systems
   - Red Hat OpenStack (rhosp) 13 & 12
   - RHOSP13 example with
     - Ceph
     - Fencing
     - instance HA
     - DNaaS (Designate) -> working only for OSP12
     - Crypto API (Barbican)
     - LBaaS (Octavia)
     - application cluster (Sahara)
     - FSaaS (Manila)
     - integration with IPA
     - integration with Operational Tools
   - RDO Packstack
   - RDO tripleo (not yet finished/tested)
   - Red Hat Cloudforms (cfme)
   - ManageIQ
   - Operational Tools (opstools - Logging, Monitoring) and usage in OpenStack
   - Identity-, Policy-, and Audit-System (IPA) and usage in OpenStack
   - nfs server and usage in RHV and Packstack
   - Ceph via OpenStack in hyperconverged (HCI) or separated flavor
   - Red Hat Virtualization (RHV) with Hosted Engine and added as infra provider to CloudForms
   - Gluster via RHV in hyperconverged manner (HCI)
   - OpenShift Aggregated Logging (oal) (not yet finished) for RHV
- a full fledged mirror for repos, images and containers using recyclable
  storage via your local machine (also allowing to use the mirror and
  internet in mixed mode and to carry the mirror to a destination system)


For my main use case Red Hat OpenStack (RHOSP) you get:

- complete setup of undercloud
- complete setup of overcloud (complete example included)
- ceph-hyperconverged example included
- ready-to-use post-config of overcloud with demo setup
- prepared for tempest run (all installed and configured, just run!)
- ansible inventory
- some testing tools and helper in ~stack/bin (basic shell scripts)
- integration with IPA and Operational Tools
- integration with CloudForms
- started integration with RHV


Planned:

- add OpenShift on OpenStack
- add integrated with Neutron, Glance and Cinder to Red Hat Enterprise Virtualization (RHV)
- use IPA as CA, DNS and NTP provider (for OSP, OPS, ...)


## Architecture

### Achtitecture as seen in the hosts and create.yml file ###

#### Layer0 - not yet implemented ####

A containing VM or container holding all other layers, mainly for testing
out the whole system from a vanilla state and probably as an easy to use
grouping system. Will be implemented when I have too much spare time
(which will never happen, so feel free to pull-request me).

#### Layer1 ####

The host that is used for VMs or containers to run. In theory it should
be possible to have multiple of them, but for now only one unified host
is implemented.

#### Layer2 ####

The VMs or containers running the products using Layer 1. For Bare Metal
we start here with the roles.

#### Layer3 ####

Things running on Layer2 products, like RHV-HE on RHV-H. Will later add
things like OpenShift on OpenStack.

### Architecture of the Ansible Roles ###

#### a) VMs or Containers ####

For the Setup of the Machines (VMs or Containers) the Ansible roles files
follows a simple structure:

>>>
**Bootstrap - Prep**
>>>

#### Bootstrap ####

This is the first time setup to be able to boot the machine and access it
via network.

#### Prep ####

This stage prepares the machine for basic usage, so hostname, networks,
and password settings happen here.

#### b) Products ####

For a certain product the Ansible roles files follow a simple structure:

>>>
**Prep - Deploy - Config**
>>>

Other stages are also sometimes there, like **add** which allows to add a
feature to a machine, like being an nfs or name server.


#### Prep ####

This stage basically prepares the machine for the product. Things like
package installaton and firewalling happen here. So mostly this step is
executed as root.

#### Deploy ####

The deployment is the actual installatin of the software product. If
possible this should happen as an unprivileged user, sometimes amended
by sudo to be able to escalate the privilege to become root.


#### Config ####

This stage, which should also happen as an unprivileged user, configures
the product for usage.

## Configuration ##

Look into [`config`](config) where there *might* be some more documentation. 

## A Basic Setup ##

There is a small and simple helper script re-create-all.sh that mimics a
complete run to setup the host machine, remove an already deployed infra,
creates the mirror first and then all other infra machines using the
tasks defined in create.yml.

### Preparation ###

To use this exapmple setup, copy
[`config/config_infrastructure.yml`](config/config_infrastructure.yml) to
`config/config_infrastructure-myinfra.yml` and edit it to suit your
needs. In the following example we are using the hosts file
[`hosts-IaC-rhosp-lab`](hosts-IaC-rhosp-lab) which will deploy the
following infrastructure:

| Node | CPUs | RAM | Disk |
|------|-----:|----:|------|
| 1 x Mirror | 2 | 1024 | default |
| 1 x IPA Server | 2 | 4096 | 100G |
| 1 x OPS Tools Server | 2 | 4096 | 100G |
| 3 x RHOSP Controller | 4 | 12288 | 1T, 1T, 400G, 400G |
| 3 x Compute | 4 | 8192 | 1T, 1T, 400G, 400G |
| 3 x RHV HCI | 4 | 12288 | 100G, 2.5T |
| **Sum** | **34** | **81GB** | |

Don't think too much about CPUs and Disks, as you can easily
overprovision both and the disks are only virtual and will only grow if
you use them. But for the RAM you should definitely have at least 80GB
for this setup.

### Download and creation of images ###

Another thing you have to prepare comes through the usage of 3rd party
software from Red Hat and others. You have to manually download all
images that have a "" in the `url:` tag in the
[`config/config-IaC-rhosp-lab.yml`](config/config-IaC-rhosp-lab.yml) and put them in
the [`binary/images/kvm-images`](binary/images/kvm-images) directory.
Essentially you only really need the the RHEL one and CFME if you intend
to install that.

As we are also using LXC you have to provide an LXC image of RHEL. You
can create this using the directions in
[`doc/create_RHEL_image.txt`](doc/create_RHEL_image.txt) and then put the
`rhel7.4-meta.tar.gz` and `rhel7.4.tar.gz` into
[`binary/images/lxc-images`](binary/images/lxc-images). Don't worry, this
only takes you 10 minutes to do so, that's why I didn't ansibelized it
until now.

**Hint:** By the way, if you want to understand why I am using LXC here,
read my OpenStack Summit Vancouver talk in
[`doc/Joachim_von_Thadden-Containerize_your_life.pdf`](doc/Joachim_von_Thadden-Containerize_your_life.pdf).

That should be all for now - if I forgot something, drop me a line.

### Deployment ###

The typical run to install this on a remote system (your lab system and
probably also the layer1 host) with screen-session and ssh-agent looks as
follows (for sure after you checked out the code on that machine):

```
ssh -aX user@remote-machine
cd lightbreeze/
ssh-agent > agent.sh
source agent.sh
ssh-add
ssh-add -l
screen -x || screen
./re-create-all.sh "-IaC" "-rhosp-lab" "-myinfra" "config-mirror.yml"
```

If you don't want to build a mirror leave the last parameter out and
don't say "y" to the 3rd phase. You will have a traditional subscribed
environment then.

You then follow the menu and say "y" to all phases, which are:

-   I) prepare the environment"
-  II) destroy all nodes"
- III) create the mirror"
-  IV) create and bootstrap VMs & Containers"
-   V) prep Layer 2 (BM starts here)"
-  VI) create Layer 2"
- VII) create Layer 3"
       
For each answer you will see the ansible-playbook command executed and
then the running of the play. So if you type "n" for all questions you
can see the commands to be run for further examination without executing
anything:

```
[thadden@jo-playground lightbreeze]$ ./re-create-all.sh "-IaC" "-rhosp-lab" "-myinfra" "config-mirror.yml"

Using: ./re-create-all.sh "-IaC" "-rhosp-lab" "-myinfra" " -e @config/config-mirror.yml"

###########################################
# I) prepare the environment
#      (Layer 0 & 1 w/o the VMs/Containers)
###########################################
command: ansible-playbook -v -i hosts-IaC-rhosp-lab -e @config/config-IaC-rhosp-lab.yml -e @config/config_infrastructure-myinfra.yml  -e @config/config-mirror.yml --tags layer0,layer1 --skip-tags starts_layer2 create.yml

Do you want to prepare the environment (y/N)? n

###########################################
# II) destroy all nodes
###########################################
command: ansible-playbook -v -i hosts-IaC-rhosp-lab -e @config/config-IaC-rhosp-lab.yml -e @config/config_infrastructure-myinfra.yml  -e @config/config-mirror.yml destroy.yml

Do you want to delete the nodes (y/N)? n

###########################################
# III) create the mirror
###########################################
command: ansible-playbook -v -i hosts-IaC-rhosp-lab -e @config/config-IaC-rhosp-lab.yml -e @config/config_infrastructure-myinfra.yml  -e @config/config-mirror.yml --extra-vars '{create_mirror:true}' --limit layer1,lxc-IaC-mirror,kvm-IaC-mirror,IaC-mirror --tags starts_layer2,bootstrap,prep_hosts,mirror --skip-tags upload_kvmimages create.yml

Do you want to create the mirror (y/N)? n

###########################################
# IV) create and bootstrap VMs & Containers
#                      (Layer 1 -> Layer 2)
###########################################
command: ansible-playbook -v -i hosts-IaC-rhosp-lab -e @config/config-IaC-rhosp-lab.yml -e @config/config_infrastructure-myinfra.yml  -e @config/config-mirror.yml --tags starts_layer2,bootstrap create.yml

Do you want to create and bootstrap the VMs & Containers (y/N)? n

###########################################
# V) prep Layer 2 (BM starts here)
###########################################
command: ansible-playbook -v -i hosts-IaC-rhosp-lab -e @config/config-IaC-rhosp-lab.yml -e @config/config_infrastructure-myinfra.yml  -e @config/config-mirror.yml --tags prep_hosts create.yml

Do you want to prepare the machines on Layer 2 (y/N)? n

###########################################
# VI) create Layer 2
###########################################
command: ansible-playbook -v -i hosts-IaC-rhosp-lab -e @config/config-IaC-rhosp-lab.yml -e @config/config_infrastructure-myinfra.yml  -e @config/config-mirror.yml --tags layer2 --skip-tags bootstrap,prep_hosts,mirror create.yml

Do you want to create all other machines on Layer 2 (Y/n)? n

###########################################
# VII) create Layer 3
###########################################
command: ansible-playbook -v -i hosts-IaC-rhosp-lab -e @config/config-IaC-rhosp-lab.yml -e @config/config_infrastructure-myinfra.yml  -e @config/config-mirror.yml --tags layer3 --skip-tags mirror create.yml

Do you want to create all other machines on Layer 3 (y/N)? n
```

The parameters of the small shell script are

```
[CUSTOMER [TAG [INFRA [CONFIG...]]]]
```

The Parameters are used in the following way for the subsequent
ansible-playbook runs:

```
-i ${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure\${INFRA}.yml -e @config/${CONFIG} ...                                                    
```

By this you can create and use per customer/project specific ansible
hosts files and configs in config/. As an example there is a customer
"Infrastructure as Code (IaC)" defined with hosts and config file, that
can be used as a starting point and for a first deployment.

**Note1:** The phase "prepare the environment" will fail on "TASK
[Gathering Facts]" on the mirror, if the mirror is not yet setup or not
running. You can ignore that.

**Note2:** When you say "y" to the "destroy all nodes" phase, the mirror
will not be destroyed and has to be destroyed manually if you need to.
Use `lxc ls` to find it and `lxc remove lxc-<CUSTOMER>-mirror` to remove
it on your layer 1 host. To remove the subscription use the Red Hat
portal. This is a feature that allows you to tear down your environment
except for the mirror to speed up the deployment time.


## The Mirror ##

You can have (and should use in the sake of speed and download bandwidth)
a mirror of all the repos defined and also of all docker images that the
OpenStack Director is using. This process also creates the needed repo
DBs and the yum repo files for the defined products.

**Note:** At the moment the mirror is only implemented as lxc image and not
as VM. This saves resources and until now I didn't have the time to
create the needed mapping of local resources into VMs (which is already
there in lxc).

### create the mirror ###

Use re-create-all.sh to create the mirror. Say "y"es to "prepare the
environment" and at least to "create the mirror". It will create a mirror
and uses mapped-in storage of your host system (by the provided example
at /home/mirror) to preserve the files for other mirrors. So all data
lands on your local system and can be shared also between different
mirrors. Note that only one mirror can be used to sync data at a time. As
this requires the docker daemon to be running on the syncing machine,
it's usually the first mirror you started that can be used to sync. To
change this you can always start the docker daemon on the required mirror
after you stopped it at the one in charge.

The mirror takes its config from [`-e
@config/config-mirror.yml`](config/config-mirror.yml) and if this it not
there, no mirror can be created. Also no mirror will be used by that.
**Note:** The latter is untested since a longer time and thus probably
subject to failure :-(

### sync repos and containers ###

Use [`re-create-all.sh`](re-create-all.sh) same as you want to create the mirror to sync. When
the mirror is already there, only syncing will happen. Repos will always
be synced into a new link-copy of the last synced directory, thus
preserving the state of an older sync by the date of the directory in
/home/mirror/repos. If you added new repos to be synced, you have to `rm
/root/<CUST>-mirror_mirror_prepared` on the mirror, so that the new repos
are added to the mirror.

RHOSP containers (docker images) are only synced if there are newer
config files. To sync RHOSP containers anew, see the next topic.

### download docker images ###

The docker images are downloaded via the
`docker-images-import-list-<OSPVER>-<DATE>.yaml` files in
[`files/`](files/). Docker images will not be re-processed once
downloaded - to do so 

```
rm /root/CnH-mirror_docker-images-import-list-<OSPVER>-<DATE>.yaml_pushed
```

on the mirror, so the same images will be synced again. This should not
change anything for sure :-)

If the OSP-Director (aka Undercloud) is running, it will be used to
create a new docker-images-import-list-<OSPVER>-<DATE>.yaml and copy it
to lightbreeze/files. This is then used at the mirror to download the
images and push them to the registry on the mirror once.


## Products ##

### Add a Product ###

To add a product you have to prepare
- `hosts-<CUST>-<TAG>`
   - add a host for the product to either [lxc-containers] or [kvm-vms]; don't forget to prepend "lxc-" or "kvm-" to the name of the host here
   - add the host for the product to [layer2]
   - add a product group [<product>] and add the host name here as well
   - add the host to the right operating system group [rhel] or [fedora] or [centos7] with its normal name and the prepended name you used in the first step
- `group_vars/<product>.yml`
   - add at least product
- `config/config-mirror.yml`
   - add a repo_overwrite for your mirror
- `config/config-<CUST>-<TAG>.yml`
   - add your product to a products or rh_products
   - add repo_files and/or repo_packages and/or create a repo file files/<product>-addtional.repo
   - add the repos to be enabled for this product to repos
   - add a product section <product> with at least an ip, a flavor and a nic1
   - you might want to add some parameters for the product as well (like <product>_user)
- `roles/...`
   - add some roles
   - I recommend using the format roles/layer2_prep_<product> and roles/layer2_deploy_<product> to separate installation and configuration/deployment parts, ideally using an unprivileged used for the deployment part
- `create.yml`
   - add the roles you created in the last step here with appropriate tags

As an easy example look at the ipa product. If you use a mirror (highly
recommended) and added new repos see *sync repos and containers* above.

To test your new product use the [`re-create-all.sh`](re-create-all.sh)
script or run the [`create.yml`](create.yml) with all parameters as
described above. 


## History ##

This project was initiated by me (j.thadden-at-redhat.com) during projects I
conducted at my employer Red Hat. The purpose is to produce a testing,
POC and learning environment on local hardware via virtualization or
(full fledged) containerization as well as Bare Metal machines.

A part of it is showing a practical way to use heavy software (like
OpenStack) or annoying software (like Cisco AnyConnect) via container
technologies on insufficient or precious resources - for testing and
preparing customer environments - or just because we can :-)

This part is about embedding software that's normally running in VMs on
your weak Linux Laptop for testing things out. As an example we use
OpenStack (until now packstack), because if you know how this one works,
everything else is a breeze!

It was originally developed for a lab at the OpenStack Summit 2018 in
Vancouver (https://github.com/jthadden/OpenStack_Summit_2018_Vancouver).

