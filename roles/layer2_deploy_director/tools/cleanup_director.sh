#!/bin/bash

# even more complete can be found here: https://raw.githubusercontent.com/racedo/vLab-Files/master/Undercloud/remove-undercloud.sh
yum -y remove "openstack*" python-rdomanager-oscplugin puppet haproxy mariadb rabbitmq-server "*swift*" "*keystone*" python-tempest
yum clean all
rm -rf /tftpboot/ /httpboot/ /var/lib/mysql/ /etc/ironic* /etc/ceilometer /etc/neutron/ /etc/glance/ /etc/keystone/ /etc/nova/ /etc/puppet/ /home/stack/undercloud-passwords.conf /etc/haproxy \
       /var/lib/rabbitmq /home/stack/.instack /var/log/keystone /var/log/nova /var/log/ironic* /var/log/glance /var/log/neutron /var/log/ceilometer /var/log/heat /etc/swift /var/log/swift \
       /srv/node/* /etc/httpd/conf.d/*keystone* /var/www/cgi-bin/keystone /var/log/httpd/keystone*
echo "You have to reboot the machine now and then reinstall the director..."

exit 0

