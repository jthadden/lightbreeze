#!/bin/bash
set -e

help() {
  cat <<EOF
Usage: $0 <tenant name> {<tenant description>|""} <user name> <email>
       $0 -d <tenant name>		# deletes a complete tenant
       $0 -d "" <user name>		# deletes a user
       $0 -d <tenant name> <user name>	# removes a user from a tenant
       
       Add a tenant and/or user. If the tenant exists, only the user will
       be added to it. Otherwise tenant and user are added. Additionally
       a keystonerc_<user name> will be created.
       
       The second form will delete a complete tenant, if one is provided
       without <user name>. If a <user name> ist provided with a tenant
       of "", the complete user will be delted. If both, <user name> and
       <tenant> are provided only the user will deleted from the tenant.
       The appropiate keystonerc file will remain and has to be deleted
       manually.
       
       Note that all users are added with the role "_member_" to the
       tenants. Note also that a username in the form name@domain will
       use keystone v3 api (aka multidomain) to cope with a user in the
       specified domain. It will therefore source ~/overcloudrc_v3 or
       ~/overcloudrc.v3.
       
EOF
  exit $1
}

source /home/stack/bin/osp-env.sh || exit 1

# get params
if [ "$1" = "-d" ]; then
  shift
  DELETE="true"
  [ "$#" -ge 1 ] || help 1
  tenant="$1"
  user="$2"
  cat <<EOF
Tenant:      $tenant
User:        $user
delete:      $DELETE
EOF
  export tenant user DELETE
else
  DELETE=""
  [ "$3" != "" ] || help 1
  tenant="$1"
  tdesc="$2"
  user="$3"
  if [ -e ~/"keystonerc_$user" ]; then
    upass=`grep OS_PASSWORD ~/keystonerc_$user|cut -d\" -f2`
    umsg="(using OLD password from keystonerc_$user)"
  else
    upass="`openssl rand -hex 16`"
    umsg=""
  fi
  uemail="$4"
  echo "Got $# parameters:"
  cat <<EOF
Tenant:      $tenant
Description: $tdesc
User:        $user
Password:    $upass $umsg
E-Mail:      $uemail"
delete:      false
EOF
  export tenant tdesc user uemail DELETE
fi

echo $user|grep "@" && domain="--domain `echo $user|cut -d@ -f2`"
user="`echo $user|cut -d@ -f1`"

if [ "$user" = "admin" ] || [ "$tenant" = "admin" ]; then
  echo "You can not tamper around with the admin user or tenant with this tool. Your should know why..."
  exit 1
fi

# work as admin
source ~/overcloudrc
if [ "$domain" ]; then
  [ -e ~/overcloudrc.v3 ] && source ~/overcloudrc.v3
  [ -e ~/overcloudrc_v3 ] && source ~/overcloudrc_v3
fi

# add tenant
echo "TENANT"
if [ ! "$DELETE" ] && ! openstack project show "$tenant"; then
  echo "  - CREATE TENANT"
  #keystone tenant-create --name "$tenant" --description "$tdesc" --enabled true
  openstack project create $domain --description "$tdesc" "$tenant"
elif [ "$DELETE" ] && [ ! "$tenant" = "" ] && [ "$user" = "" ]; then
  echo "  - DELETE TENANT"
  #keystone tenant-delete "$tenant"
  openstack project delete $domain "$tenant"
fi
# add user
echo "USER"
if [ ! "$DELETE" ] && ! openstack user show "$user"; then
  echo "  - CREATE USER"
  #keystone user-create --name "$user" --email "$uemail" --pass "$upass" --tenant "$tenant" --enabled true
  openstack user create --email "$uemail" --password "$upass" --project "$tenant" --enable "$user"
  # keystonerc_$user
  [ "$umsg" = "" ] && cat <<EOF >~/keystonerc_"$user"
# Clear any old environment that may conflict.
`grep "do unset" ~/overcloudrc`
export OS_NO_CACHE=True
`grep COMPUTE_API_VERSION= ~/overcloudrc`
export OS_USERNAME="$user"
`grep no_proxy= ~/overcloudrc`
`if [ "$OSPVER" -ge 13 ]; then echo "export OS_USER_DOMAIN_NAME=Default"; else echo "unset OS_USER_DOMAIN_NAME"; fi`
`grep OS_VOLUME_API_VERSION= ~/overcloudrc`
export OS_CLOUDNAME=overcloud
`grep OS_AUTH_URL ~/overcloudrc`
`grep NOVA_VERSION= ~/overcloudrc`
`grep OS_IMAGE_API_VERSION= ~/overcloudrc`
export OS_PASSWORD="$upass"
`if [ "$OSPVER" -ge 13 ]; then echo "export OS_PROJECT_DOMAIN_NAME=Default"; else echo "unset OS_PROJECT_DOMAIN_NAME"; fi`
`grep OS_IDENTITY_API_VERSION= ~/overcloudrc`
export OS_PROJECT_NAME="$tenant"
export OS_TENANT_NAME="${OS_PROJECT_NAME}"
export OS_AUTH_TYPE=password
`grep PYTHONWARNINGS= ~/overcloudrc`

export PS1="($user@${OS_CLOUDNAME}) [\u@\h \W]\$ "

EOF
  grep "OS_PROJECT_NAME=" ~/overcloudrc || echo 'export OS_PROJECT_NAME="admin"' >> overcloudrc
elif [ "$DELETE" ] && [ "$tenant" = "" ] && [ ! "$user" = "" ]; then
  echo "  - DELETE USER"
  #keystone user-delete "$user"
  openstack user delete "$user"
elif [ "$DELETE" ] && [ ! "$tenant" = "" ] && [ ! "$user" = "" ]; then
  echo "  - REMOVE USER FROM TENANT"
  #ROLES="`keystone user-role-list --tenant=demo --user=demo|tail -n +3 |awk '{print $4}'|grep -v ^$`"
  ROLES="`openstack role assignment list --user "$user" --project "$tenant" --names|tail -n +3 |awk '{print $2}'|grep -v ^$`"
  for ROLE in $ROLES; do
    #keystone user-role-delete --user="$user" --role="$ROLE" --tenant="$tenant"
    openstack role remove --project "$tenant" --user "$user" "$ROLE"
  done
fi

# with -d we are done.
[ $DELETE ] && { echo "done." && exit 0; }

# add userroles to tenant
ROLES="_member_ heat_stack_owner heat_stack_user"
ROLES="_member_"
for role in $ROLES; do
  echo "  - ADD ROLE $role TO USER $user IN PROJECT $tenant"
  #if ! keystone user-role-list --user "$user" --tenant "$tenant"|tail -n +3|awk '{print $4}'|grep "$role"; then
  if ! openstack role assignment list --user "$user" --project "$tenant" --names|tail -n +3 |awk '{print $2}'|grep "$role"; then
    if openstack role list|grep $role; then
      #keystone user-role-add --user="$user" --role="$role" --tenant="$tenant"
      openstack role add --project "$tenant" --user "$user" "$role"
    fi
  fi
done
#keystone user-role-list --user "$user" --tenant "$tenant"
openstack role assignment list --user "$user" --project "$tenant" --names

# Switch credentials
. ~/keystonerc_"$user"
# Switch to the right tenant
[ "$tenant" = "" ] || export OS_TENANT_NAME="$tenant"

# Create an ssh keypair
[ -e id_rsa_"$user".pub ] || ssh-keygen -t rsa -b 2048 -N '' -f id_rsa_"$user"

# Add a ssh keypair
nova keypair-add --pub-key id_rsa_"$user".pub "$user"

BINDIP="`grep DnsServers templates/network-environment*.yaml|cut -d\\" -f2|cut -d, -f1|head -1`"	# for mc's sake "
[ "$BINDIP"="" ] && BINDIP="8.8.8.8"

# Create security rules: Make sure we allow ICMP and SSH traffic to instances
neutron security-group-rule-create --protocol icmp default
neutron security-group-rule-create --protocol tcp \
  --port-range-min 22 --port-range-max 22 default

# Create additional tenant private network
neutron net-create test_net
neutron subnet-create --name test_subnet \
  --dns-nameserver $BINDIP --enable-dhcp test_net 10.0.0.0/24

# Create additional tenant router for additional tenant private network
[ "$PUBGW" ] || PUBGW="public"
neutron router-create test_router
neutron router-gateway-set test_router $PUBGW
neutron router-interface-add test_router test_subnet

echo "done."
