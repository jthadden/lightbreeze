#!/bin/bash

help() {
cat <<EOF
Usage: $0 [-h | --wipe | --all]

This script deletes the stack and resets the nodes state to be installed again.
Use --wipe to dd the start of the disks as well. Use --all to remove the ironic
nodes as well, so that you have to  re-inventory/introspect your setup. Both
parameters can be combined.

EOF
exit 0
}

[ "$1" = "-h" ] && help

osp-status.sh
source ~/stackrc || exit 1

if [ "$1" = "--wipe" ] || [ "$2" = "--wipe" ]; then
  echo "Attention. This will wipe the first 4MB of ervery disk and partition of *all* disks of every node accessible."
  read -p "Are you shure you want to continue [yes|no]? " answer
  if [ "$answer" = "yes" ]; then
    for node in `nova list|awk '/Running/ {print $2}'`; do
      echo -e "\n##################################################################################################"
      echo "# Deleting *all* data on node `nova show $node|grep " name "|awk '{print $4}'` at `date`..."
      echo -e "##################################################################################################\n"
      PSSH="-oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -q"
      ssh $PSSH heat-admin@`echo \`nova show $node|grep ctlplane|cut -d\| -f3\`` 2>&1 sudo -i <<"EOF"
        for DEV in `ls -r1 /dev/disk/by-path`; do
          dd if=/dev/zero of=${DEV} bs=1M count=4
          sync
        done
EOF
    done
  fi
fi

if [ "`openstack stack list | tee $0.$$ | grep " overcloud "`" = "" ]; then
  echo "no overcloud to delete..."
else
  openstack stack delete overcloud
  sleep 60s
  while openstack stack list | tee $0.$$ | grep " overcloud "; do
    grep "DELETE_IN_PROGRESS" $0.$$ >/dev/null || openstack stack delete --yes overcloud
    sleep 10s
  done
  rm $0.$$
  openstack stack list
fi

for n in `ironic node-list|grep -v "available\|manageable"|awk '/None/ {print $2}'`; do
  ironic node-set-maintenance $n off
  #ironic node-set-provision-state $n deleted
done

echo "# set all nodes to a managed/active state"
#for node in $(openstack baremetal node list|awk '{print $2}'|grep -v "UUID\|^$") ; do
#  #openstack baremetal node manage $node
#  ironic node-set-provision-state $node active
#done

echo "# removing lock files, ansible-inventory.yaml, osp-depoy.out and osp-setup.out"
cd ~
rm {{ inventory_hostname }}_overcloud_deployed {{ inventory_hostname }}_overcloud_prepared {{ inventory_hostname }}_overcloud_configured ansible-inventory.yaml osp-deploy.out osp-setup.out .ssh/known_hosts

if [ "$1" = "--all" ] || [ "$2" = "--all" ]; then
  read -p "Attention. You will have to completely re-inventory/introspect your setup. Are you shure [yes|no]? " answer
  if [ "$answer" = "yes" ]; then
    rm ~/instackenv.json
    for n in `ironic node-list|grep -v "available\|manageable"|awk '/None/ {print $2}'`; do
      ironic node-set-maintenance $n on
      ironic node-set-provision-state $n deleted
    done
    for n in `ironic node-list|awk '/None/ {print $2}'`; do
      ironic node-update $n remove instance_uuid
      ironic node-delete $n
    done
    for n in `nova list|head -n -1|tail -n +4`; do
      nova delete $n
    done
  fi
fi

osp-status.sh
