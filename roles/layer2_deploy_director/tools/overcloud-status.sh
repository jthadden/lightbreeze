#!/bin/bash
source ~/overcloudrc
for n in "hypervisor" "compute service" "volume service" "network agent" "network" "subnet" "image" "server"; do
  openstack $n list --long
done
