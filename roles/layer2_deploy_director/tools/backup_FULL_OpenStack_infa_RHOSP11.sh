#!/bin/bash

[[ ${USER} != stack ]] && {
	echo -e "\nYou must be logged in as the stack user to run this script.\n"
	exit
	}

cd /home/stack  ; source ~/stackrc

echo "Creating variables..."
ONE_CTL=$(openstack server list |grep $(ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$(openstack port list | awk -F\' '/control_virtual_ip/ {print $2}') hostname -s) |awk '{print $8}' | sed 's/ctlplane=//g')
ONE_CTL="4.239.239.161"

# Find the controller which is currently running as master
TARGET_MARIADB=$(ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$(openstack port list | awk -F\' '/control_virtual_ip/ {print $2}') hostname -s)
HOST_MARIADB=$(openstack server list |grep $TARGET_MARIADB | awk -F\| '{print $5}' | sed 's/ ctlplane=//g' | awk -F' ' '{print $1}')
HOST_MARIADB=$(openstack port list | awk -F\' '/control_virtual_ip/ {print $2}')

VIP_MONGODB=$(ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$(openstack port list | awk -F\' '/control_virtual_ip/ {print $2}') "cat  /etc/mongod.conf | grep bind_ip | sed 's/bind_ip = //g'")
HOST_MONGODB=$(ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL "sudo mongo --host $VIP_MONGODB --port 27017 --eval 'printjson(db.runCommand({\"isMaster\": 1}))' | grep primary | sed 's/\t\"primary\" : \"//g' | sed 's/:27017\",//g'")	# '

HOST_REDIS=$(ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$HOST_MARIADB "cat /etc/redis.conf | grep 'bind' | grep -v ^# | sed 's/bind //g'")
PASS_REDIS=$(ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$HOST_MARIADB "cat /etc/redis.conf | grep 'masterauth' | grep -v ^# | sed 's/masterauth //g'")

echo "TARGET_MARIADB=$TARGET_MARIADB HOST_MARIADB=$HOST_MARIADB VIP_MONGODB=$VIP_MONGODB HOST_MONGODB=$HOST_MONGODB HOST_REDIS=$HOST_REDIS PASS_REDIS=$PASS_REDIS"

function backup_undercloud {

# Backup Undercloud
echo "Backing up Director"
mkdir -p /tmp/osp_backup/director/


mkdir -p /tmp/osp_backup/director/mariadb
source /home/stack/stackrc

sudo mysqldump -u root --opt --single-transaction --all-databases > /home/stack/undercloud-all-databases.sql ; sudo mv /home/stack/undercloud-all-databases.sql /root/undercloud-all-databases.sql
#sudo mysql -u root -e "SELECT CONCAT('\"SHOW GRANTS FOR ''',user,'''@''',host,''';\"') FROM mysql.user where length(user) > 0"  -s -N | xargs -n1 mysql -u root -s -N -e | sed 's/$/;/'> /tmp/osp_backup/director/mariadb/director-grants-`date +%F`-`date +%T`.sql

sudo tar --xattrs -czf /tmp/osp_backup/director/director-backup-`date +%F`.tar.gz /root/undercloud-all-databases.sql /etc/my.cnf.d/server.cnf  /etc/hosts /var/lib/glance/images /srv/node /home/stack /etc/pki/tls/ /etc/pki/tls/certs/undercloud*.pem /etc/pki/ca-trust/source/anchors/*.pem
}


function backup_mariadb {
# Backup Overcloud MariaDB
echo "Backing up Overcloud MariaDB"
mkdir -p /tmp/osp_backup/mariadb

cat << 'EOF' > /tmp/backupmariadb.$$
#!/bin/bash
mkdir -p /home/stack/backup_mariadb
cd /home/stack/backup_mariadb
sudo mysql -u root -e "select distinct table_schema from information_schema.tables where engine='innodb' and table_schema != 'mysql';" -s -N | xargs mysqldump -u root --single-transaction --databases > backup_mariadb.sql
sudo mysql -u root -e "SELECT CONCAT('\"SHOW GRANTS FOR ''',user,'''@''',host,''';\"') FROM mysql.user where length(user) > 0"  -s -N | xargs -n1 mysql -u root -s -N -e | sed 's/$/;/' > backup_mariadb-grants.sql
EOF

scp -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null  /tmp/backupmariadb.$$ stack@$HOST_MARIADB:/home/stack/backupmariadb.sh
ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$HOST_MARIADB 'sudo chmod +x /home/stack/backupmariadb.sh ; sudo bash backupmariadb.sh 2>/dev/null'
scp -r -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$HOST_MARIADB:/home/stack/backup_mariadb/* /tmp/osp_backup/mariadb
ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$HOST_MARIADB 'sudo rm -rf /home/stack/backup_mariadb ; sudo rm -rf /home/stack/backupmariadb.sh'

rm -f /tmp/backupmariadb.$$
}

function backup_mongodb {
# Backup MongoDB
echo "Backing up Overcloud MongoDB"
mkdir -p /tmp/osp_backup/mongodb
ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL  mkdir -p /home/stack/backup_mongodb
ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL  sudo mongodump --oplog --host $HOST_MONGODB --out /home/stack/backup_mongodb

scp -r -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL:/home/stack/backup_mongodb/* /tmp/osp_backup/mongodb
ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL sudo rm -rf /home/stack/backup_mongodb
}


function backup_redis {
# Backup Redis
echo "Backing up Overcloud Redis"
mkdir -p /tmp/osp_backup/redis
ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL "sudo redis-cli -a $PASS_REDIS -h $HOST_REDIS save ; mkdir -p /home/stack/backup_redis ; sudo cp /var/lib/redis/dump.rdb /home/stack/backup_redis ; sudo chown -R stack:stack  /home/stack/backup_redis"

scp -r -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL:/home/stack/backup_redis/* /tmp/osp_backup/redis
ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@$ONE_CTL sudo rm -rf /home/stack/backup_redis
}

function backup_files {
# Backup Files
echo "Backing up Overcloud files"
mkdir -p /tmp/osp_backup/osp-files
cat << 'EOF' > /home/stack/backupfiles.$$
#!/bin/bash
hostname="$(`echo hostname` | cut -d "." -f1)"
current_date="$(`echo date '+%Y.%m.%d-%H.%M.%S'`)"
backup_filename="${hostname}-backupfile-${current_date}.tar.gz"
mkdir -p /home/stack/backups

cd /home/stack/backups

sudo tar -zcvf ${backup_filename} \
    	/etc/nova \
    	/var/log/nova \
    	/var/lib/nova \
    	--exclude /var/lib/nova/instances \
    	/etc/glance \
    	/var/log/glance \
    	/var/lib/glance \
    	/etc/keystone \
    	/var/log/keystone \
    	/var/lib/keystone \
    	/etc/httpd \
    	/etc/cinder \
    	/var/log/cinder \
    	/var/lib/cinder \
    	/etc/heat \
    	/var/log/heat \
    	/var/lib/heat \
    	/var/lib/heat-config \
    	/var/lib/heat-cfntools \
    	/etc/rabbitmq \
    	/var/log/rabbitmq \
    	/var/lib/rabbitmq \
    	/etc/neutron \
    	/var/log/neutron \
    	/var/lib/neutron \
    	/etc/corosync \
    	/etc/haproxy \
    	/etc/logrotate.d/haproxy \
    	/var/lib/haproxy \
    	/etc/openvswitch \
    	/var/log/openvswitch \
    	/var/lib/openvswitch \
    	/etc/ceilometer \
    	/etc/sysconfig/memcached

sha256sum /home/stack/backups/${backup_filename} > /home/stack/backups/${backup_filename}.sha256

echo "CREATED FILE BACKUP: ${backup_filename}"
EOF


ip=$(openstack server list -c Networks  | sed -n 's/.*ctlplane=\(.*\)\s.*/\1/p')


for i in $ip
do
	scp -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null  /home/stack/backupfiles.$$ stack@${i}:/home/stack/backupfiles.sh

	remote_file=$( ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null  stack@${i} "sudo chmod 755 /home/stack/backupfiles.sh; sudo /home/stack/backupfiles.sh" \
             	| sed -n 's/^CREATED FILE BACKUP:\s*\(.*\)/\1/p' )

	scp -r -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@${i}:/home/stack/backups/${remote_file}* /tmp/osp_backup/osp-files/

	scp -r -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@${i}:/home/stack/backups/${remote_file}* /tmp/osp_backup/osp-files
  ssh -o LogLevel=quiet -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null stack@${i} "rm -rf /home/stack/backups ; rm /home/stack/backupfiles.sh"

done
sudo rm -f /home/stack/backupfiles.$$
}

#backup_undercloud
backup_mariadb
backup_mongodb
backup_redis
#backup_files


backupdate=`date +%F`-`date +%T`
mkdir -p /home/stack/backups/backup_$backupdate
mv /tmp/osp_backup/* /home/stack/backups/backup_$backupdate/
#tar -czvf /home/stack/backup_$backupdate.tar.gz /home/stack/backups/backup_$backupdate

rm -rf /tmp/osp_backup
