#!/bin/bash
# name[0]:VMs[1]:CPUs[2]:MEM[3]:HDD[4]:NICs[5]
source /home/stack/bin/osp-env.sh || exit 1
source stackrc || exit 1

# Become admin
source ~/overcloudrc

BINDIP="`grep "^[\ ]*DnsServers:" templates/network-environment*.yaml|cut -d\\" -f2|cut -d, -f1|head -1`"	# for mc's sake "
{%if networks.floatingip is defined %}
EXTNETCIDR="{{ networks.floatingip.network }}/{{ networks.floatingip.netmask }}"
EXTNETGW="{{ networks.floatingip.gateway }}"
EXTVLAN="{{ networks.floatingip.vlan }}"
{% else %}
EXTNETCIDR="`grep "^[\ ]*ExternalNetCidr:" templates/network-environment*.yaml|cut -d: -f2|xargs`"
EXTNETGW="`grep "^[\ ]*ExternalInterfaceDefaultRoute:" templates/network-environment*.yaml|cut -d: -f2|xargs`"
EXTVLAN="`grep "^[\ ]*ExternalNetworkVlanID:" templates/network-environment*.yaml|awk '{print $2}'`"
{% endif %}

echo -e "OSPVER=$OSPVER\nEXTPOOLSTART=$EXTPOOLSTART\nEXTPOOLEND=$EXTPOOLEND\nBINDIP=$BINDIP\nEXTNETCIDR=$EXTNETCIDR\nEXTNETGW=$EXTNETGW\nEXTVLAN=$EXTVLAN\n\n"
read -p "Are you satisfied [Ctrl+c to cancel]? "
echo

if grep "NeutronExternalNetworkBridge:" templates/network-environment*.yaml|grep "\"''\""; then
  # in case of NeutronExternalNetworkBridge: "''" to enable multiple external networks or VLANs we add a network public as flat ro vlan network
  if [ "$EXTVLAN" ]; then
    PUBGW="public${EXTVLAN}"
    neutron net-create $PUBGW --router:external --provider:network_type vlan --provider:physical_network datacentre --provider:segmentation_id $EXTVLAN
  else
    PUBGW="public_flat"
    neutron net-create $PUBGW --router:external --provider:network_type flat --provider:physical_network datacentre
  fi
  neutron subnet-create $PUBGW  ${EXTNETCIDR} \
    --name ${PUBGW}_subnet --enable-dhcp=False \
    --allocation-pool start=${EXTPOOLSTART},end=${EXTPOOLEND} \
    --gateway ${EXTNETGW} --dns-nameserver $BINDIP
else
  # create the external network
  PUBGW="public"
  neutron net-create $PUBGW --router:external
  neutron subnet-create $PUBGW ${EXTNETCIDR} \
    --name ${PUBGW}_subnet --enable-dhcp=False \
    --allocation-pool start=${EXTPOOLSTART},end=${EXTPOOLEND} \
    --gateway ${EXTNETGW} --dns-nameserver $BINDIP
  #neutron net-create nova --router:external --provider:network_type flat --provider:physical_network datacentre
  #neutron subnet-create --name nova --enable_dhcp=False --allocation-pool=start=${EXTPOOLSTART},end=${EXTPOOLEND} --gateway=${EXTNETGW} nova ${EXTNETCIDR}
fi
export PUBGW

grep "^export PUBGW=$PUBGW" /home/stack/bin/osp-env.sh &>/dev/null || echo "export PUBGW=$PUBGW" >> /home/stack/bin/osp-env.sh

# create the internal network
neutron net-create internal
neutron subnet-create internal 192.168.0.0/24 --name internal_subnet
#neutron net-create default
#neutron subnet-create --name default --gateway 172.20.1.1 default 172.20.0.0/16

# create the router
neutron router-create internal_router
neutron router-gateway-set internal_router $PUBGW
neutron router-interface-add internal_router internal_subnet

# Create a disk image if it does not exist
{% for item in [osp_testimage, ovirt_testimage] %}
if ! openstack image list | grep {{ item.name|default(item.alias) }} &>/dev/null; then
  wget -c -O {{ item.alias }} -c {% if repo_overwrite is defined and repo_overwrite[product] is defined %}http://{{ mirror.ip }}/{{ mirror.maps.images.alias }}/{{ item.alias }}{% else %}{{ item.url }}{% endif %}

  openstack image create \
         --public --container-format bare --disk-format {{ item.format }} \
         --min-disk {{ item.min_disk }} --min-ram {{ item.min_ram }} {{ item.name|default(item.alias) }} < `pwd`/{{ item.alias }}
  #{% if repo_overwrite is defined and repo_overwrite[product] is defined %}rm {{ item.alias }}{% endif %}

fi
{% endfor %}

if ! openstack image list | grep rhel7 &>/dev/null; then
  sudo yum -y install rhel-guest-image-7
  echo "# be patient, while RHEL is uploaded..."
  for image in /usr/share/rhel-guest-image-7/rhel-guest-image-7*.qcow2; do
    if grep ": rbd" ~/templates/storage-environment.yaml || [ -e ~/templates/*ceph-external*.yaml ]; then
      echo "  # Ceph storage backend detected"
      RAW="`basename $image .qcow2`.raw"
      echo "    - converting image to RAW"
      qemu-img convert -f qcow2 -O raw $image ~/$RAW
      #glance image-create --name=rhel7 --disk-format=raw --container-format=bare --is-public=true < ~/$RAW
      #glance image-create --name=rhel7 --disk-format=raw --container-format=bare --visibility=public < ~/$RAW
      echo "    - uploading image"
      openstack image create --disk-format=raw --public --min-disk 10 --min-ram 1024 rhel7 < ~/$RAW
    else
      echo "  - uploading image"
      openstack image create --disk-format=qcow2 --public --min-disk 10 --min-ram 1024 rhel7 < $image
    fi
  done
  echo "done."
fi

# since RHOSP10 we need to create the standard flavors by hand
if [ "$OSPVER" -ge "10" ]; then
  nova flavor-create m1.tiny 1 512 1 1
  nova flavor-create m1.small 2 2048 20 1
  nova flavor-create m1.medium 3 4096 40 2
  nova flavor-create m1.large 4 8192 80 4
  nova flavor-create m1.xlarge 5 16384 160 8
fi

# Create some additional flavors for testing
nova flavor-create m1.normal auto 1024 10 1
nova flavor-create m1.milli auto 256 1 1
nova flavor-create m1.micro auto 128 1 1
nova flavor-create m1.nano auto 64 1 1
#nova flavor-create m1.pico auto 32 1 1

openstack flavor list

echo -e "\n\n#######################################################"
echo "# make demo tenant and user"
echo "#######################################################"
[ "$1" = "--nodemo" ] || mkTenant.sh demo "Demo Tenant" demo "demo@localhost" 2>&1 | grep -v "DeprecationWarning:"
#echo "export PS1='[\u@\h \W]$ '" >> ~/overcloudrc
sed -i "/export CLOUDPROMPT_ENABLED=1/d" ~/overcloudrc
sed -i -e "s/^\(.*CLOUDPROMPT_ENABLED:-.*\)$/\1\n    export PS1='[\\\u@\\\h \\\W]$ '/" ~/overcloudrc
sed -i -e "s/(\\\/(admin@\\\/" ~/overcloudrc
