#!/bin/bash

##########
# DIR host
# - sync time
##########
# on the director synchronize the time
cd
bin/osp-prep-ansible-inventory.sh
source bin/osp-env.sh
#sudo ntpdate -s $TIME
while true; do sudo ntpdate -s $TIME && break;date;done
chronyc sources -v
date
ansible all -b -m shell -a "service ntpd stop ;ntpdate -s $TIME;service ntpd start;date"
source overcloudrc
nova service-list
#ceph-status.sh|head -20
