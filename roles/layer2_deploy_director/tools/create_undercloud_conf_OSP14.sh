#!/bin/bash

source ~stack/bin/osp-env.sh || exit 1
export OSP_VERSION="$OSPVER"
export SUB_TYPE="rhos-release"

function configure_undercloud()
{

  echo "Configuring undercloud.conf"

  sudo -u stack cp /usr/share/instack-undercloud/undercloud.conf.sample /home/stack/undercloud.conf || { echoerr "Failed to copy sample undercloud file to /home/stack/"; return 1; }

  sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT local_ip {{ director.prov_cidr }}
  sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT local_interface {{ director.prov_nic }}
  sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT masquerade_network {{ director.prov_cidr }}

  if [ $OSP_VERSION -le 13 ]; then
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT undercloud_public_vip {{ (( director.prov_cidr.split('/')[0] | ipaddr('int')) + 1) | ipaddr }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT undercloud_admin_vip {{ (( director.prov_cidr.split('/')[0] | ipaddr('int')) + 2) | ipaddr }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT dhcp_start {{ prov_dhcp_start }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT dhcp_end {{ prov_dhcp_end }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT network_cidr {{ provnet_cidr }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT network_gateway {{ provnet_gw }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT discovery_iprange {{ prov_introspect_start }},{{ prov_introspect_end }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT inspection_iprange {{ prov_introspect_start }},{{ prov_introspect_end }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT generate_service_certificate false

  # OSP14+ some undercloud.conf parameters change and the undercloud is now containerised
  else
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT undercloud_public_host {{ (( director.prov_cidr.split('/')[0] | ipaddr('int')) + 1) | ipaddr }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT undercloud_admin_host {{ (( director.prov_cidr.split('/')[0] | ipaddr('int')) + 2) | ipaddr }}
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT clean_nodes false
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT container_images_file /home/stack/undercloud-templates/containers-prepare-parameter.yaml
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT custom_env_files /home/stack/undercloud-templates/ntp.yaml
    sudo -u stack crudini --set /home/stack/undercloud.conf DEFAULT docker_insecure_registries docker-registry.engineering.redhat.com

    sudo -u stack crudini --set /home/stack/undercloud.conf ctlplane-subnet local_subnet ctlplane-subnet
    sudo -u stack crudini --set /home/stack/undercloud.conf ctlplane-subnet masquerade true
    sudo -u stack crudini --set /home/stack/undercloud.conf ctlplane-subnet cidr {{ provnet_cidr }}
    sudo -u stack crudini --set /home/stack/undercloud.conf ctlplane-subnet gateway {{ provnet_gw }}
    sudo -u stack crudini --set /home/stack/undercloud.conf ctlplane-subnet inspection_iprange {{ prov_introspect_start }},{{ prov_introspect_end }}
    sudo -u stack crudini --set /home/stack/undercloud.conf ctlplane-subnet dhcp_start {{ prov_dhcp_start }}
    sudo -u stack crudini --set /home/stack/undercloud.conf ctlplane-subnet dhcp_end {{ prov_dhcp_end }}

    sudo -u stack mkdir /home/stack/undercloud-templates || { echoerr "Failed to create /home/stack/undercloud-templates directory"; return 1; }

    echo "Generating containers-prepare-parameter.yaml..."
    sudo -u stack openstack tripleo container image prepare default  --local-push-destination  --output-env-file /home/stack/undercloud-templates/containers-prepare-parameter.yaml || \
    { echoerr "Failed to generate /home/stack/undercloud-templates/containers-prepare-parameter.yaml "; return 1; }
    # Set container registry to internal one if using rhos-release
    if [ "$SUB_TYPE" = "rhos-release" ]; then
      sudo -u stack sed -i "s?registry.access.redhat.com/rhosp14?docker-registry.engineering.redhat.com/rhosp${OSP_VERSION}?" /home/stack/undercloud-templates/containers-prepare-parameter.yaml
    fi


    echoinfo "Generating ntp.yaml"
    sudo -u stack cat << EOF > /home/stack/undercloud-templates/ntp.yaml
parameter_defaults:
  NtpServer: '{{ provnet_ntp1 }}'
EOF
  fi

}

configure_undercloud
mv /home/stack/undercloud.conf /home/stack/undercloud.conf-generated
