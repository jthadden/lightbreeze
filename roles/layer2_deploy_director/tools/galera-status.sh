#!/bin/bash

source ~stack/stackrc

NODES="`nova list|grep \"control\"|awk '{print $12}'|cut -d= -f2`"
for NODE in ${NODES}; do ssh -t -oStrictHostKeyChecking=no heat-admin@$NODE <<EOF
  echo "============= openstack controller cluster state on node $NODE ============="
  sudo -i <<SEND
    mysql -u root -e "show status like 'wsrep%'" | grep "wsrep_cluster_status\|local_state_comment\|incoming_addresses\|evs_state\|cluster_size\|wsrep_ready\|last_committed"
    mysql -u root -e "SHOW GLOBAL VARIABLES LIKE 'wsrep_provider_options';"|tr \; \\\\\n|grep pc.weight
SEND
EOF
done 2>/dev/null
