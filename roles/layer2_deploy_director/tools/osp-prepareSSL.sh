#!/bin/bash

help() {
cat <<EOF
Usage: $0 [-h | --use-fqdn]

This script creates a CA and the apropiate certs for the overcloud. All
files reside in ~stack/certs/myCA.

To use the FQDN instead of the IP use --use-fqdn.

EOF
exit 0
}

[ "$1" = "-h" ] && help

source /home/stack/bin/osp-env.sh || exit 1
source stackrc || exit 1

declare -A CCN

source ~/ssl-config.input

#echo "not yet ready to use" && exit

echo "# initializing the signing host..."
mkdir -p ~/certs/myCA
cd ~/certs/myCA
mkdir certs crl newcerts private templates
touch index.txt
[ -e serial ] || echo '1000' > serial

echo "# creating the CA..."
[ -e ca.key.pem ] || openssl genrsa -out ca.key.pem 4096
[ -e ca.crt.pem ] || openssl req -key ca.key.pem -new -x509 -days 7300 -extensions v3_ca -out ca.crt.pem <<EOF
$CCountry
$CState
$CCity
$COrg
$COU
${CCN["CA"]}
$CEmail

EOF

unset CCN["CA"]

echo "# activating CA on the local host..."
sudo cp ca.crt.pem /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract

echo "# adding the Certificate Authority to Clients..."
cat <<EOF

For any clients aiming to communicate with the undercloud or overcloud
using SSL/TLS, copy the certificate authority file to each client that
aims to access your Red Hat OpenStack Platform environment and add it
to the certificate authority trust bundle:

$ scp ~/certs/myCA/ca.crt.pem root@$NODE:/etc/pki/ca-trust/source/anchors/
$ ssh root@$NODE update-ca-trust extract
EOF

if [ ! -e openssl.cnf ]; then
  echo "# Configuring the signing request basics in openssl.cnf..."
  cp /etc/pki/tls/openssl.cnf .

  crudini --get openssl.cnf " CA_default " dir
  crudini --set openssl.cnf " CA_default " dir $PWD

  crudini --get openssl.cnf " req_distinguished_name " countryName_default
  crudini --set openssl.cnf " req_distinguished_name " countryName_default "$CCountry"

  crudini --get openssl.cnf " req_distinguished_name " stateOrProvinceName_default
  crudini --set openssl.cnf " req_distinguished_name " stateOrProvinceName_default "$CState"

  crudini --get openssl.cnf " req_distinguished_name " localityName_default
  crudini --set openssl.cnf " req_distinguished_name " localityName_default "$CCity"

  crudini --get openssl.cnf " req_distinguished_name " 0.organizationName_default
  crudini --set openssl.cnf " req_distinguished_name " 0.organizationName_default "$COrg"

  crudini --get openssl.cnf " req_distinguished_name " organizationalUnitName_default
  crudini --set openssl.cnf " req_distinguished_name " organizationalUnitName_default "$COU"

  crudini --get openssl.cnf " req_distinguished_name " emailAddress_default
  crudini --set openssl.cnf " req_distinguished_name " emailAddress_default "$CEmail"
fi

# CN Undercloud
CCN["Undercloud"]="`crudini --get ~/undercloud.conf DEFAULT undercloud_public_vip`"
[ "$1" = "--use-fqdn" ] && CCN["Undercloud"]="`hostname -f`"

# CN Overcloud
CCN["Overcloud"]="`grep PublicVirtualFixedIPs ~/templates/network-environment.yaml | cut -d\' -f4`"
[ "$CCNo" = "" ] && CCNo="`grep ExternalAllocationPools ~/templates/network-environment.yaml | cut -d\' -f4`"
[ "$1" = "--use-fqdn" ] && CCN["Overcloud"]="overcloud.$MYDOMAIN"

# CN OPS
source ~/OPS10/scripts/ops-env.sh
CCN["OPS"]="$MYIP"
source ~/bin/osp-env.sh
[ "$1" = "--use-fqdn" ] && echo "FQDN for OPS is not yet implemented..."

for n in ${!CCN[@]}; do
  echo "# creating the SSL/TLS key for $n..."
  [ -e $n.key.pem ] || openssl genrsa -out $n.key.pem 2048

  echo "# Configuring the signing request server specifics in openssl.cnf..."

  crudini --get openssl.cnf " req_distinguished_name " commonName_default
  crudini --set openssl.cnf " req_distinguished_name " commonName_default "${CCN[$n]}"

  crudini --get openssl.cnf " alt_names " IP.1
  crudini --set openssl.cnf " alt_names " IP.1 "${CCN[$n]}"

  crudini --get openssl.cnf " alt_names " DNS.1
  crudini --set openssl.cnf " alt_names " DNS.1 "${CCN[$n]}"

  #crudini --get openssl.cnf " alt_names " DNS.2
  #crudini --set openssl.cnf " alt_names " DNS.2 <Hostname of Public Endpoint>

  echo "# Creating the SSL/TLS certificate signing request for Server $n..."
  [ -e $n.csr.pem ] || openssl req -config openssl.cnf -key $n.key.pem -new -out $n.csr.pem <<EOF
$CCountry
$CState
$CCity
$COrg
$COU
${CCN[$n]}
$CEmail



EOF


  echo "# creating the SSL/TLS certificate for $n..."
  [ -e $n.crt.pem ] || openssl ca -config openssl.cnf -extensions v3_req -days 3650 -in $n.csr.pem -out $n.crt.pem -cert ca.crt.pem -keyfile ca.key.pem <<EOF
y
y

EOF
done

echo "# prepare for using the Certificate with the Undercloud..."
echo "# not yet implemented, see https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/12/html/director_installation_and_usage/appe-ssltls_certificate_configuration#using_the_certificate_with_the_undercloud"
#cat Undercloud.crt.pem Undercloud.key.pem > ../undercloud.pem
sudo cp /etc/pki/tls/certs/undercloud-*.pem ../undercloud.pem
cp /etc/pki/ca-trust/source/anchors/cm-local-ca.pem ../undercloud-cm-local-ca.pem

echo "# using the Certificate with the Overcloud..."
cat <<EOF > templates/enable-tls-environment.yaml
# title: Enable SSL on OpenStack Public Endpoints
# description: |
#   Use this environment to pass in certificates for SSL deployments.
#   For these values to take effect, one of the tls-endpoints-*.yaml environments
#   must also be used.
parameter_defaults:
  # The content of the SSL certificate (without Key) in PEM format.
  # Type: string
  SSLCertificate: |
`sed -n -e 's/^/     /' -e '/-----BEGIN CERTIFICATE-----/,$p' Overcloud.crt.pem`

  # The content of an SSL intermediate CA certificate in PEM format.
  # Type: string
  SSLIntermediateCertificate: ''

  # The content of the SSL Key in PEM format.
  # Mandatory. This parameter must be set by the user.
  # Type: string
  SSLKey: |
`sed -n -e 's/^/     /' -e '/-----BEGIN RSA PRIVATE KEY-----/,$p' Overcloud.key.pem`

  # ******************************************************
  # Static parameters - these are values that must be
  # included in the environment but should not be changed.
  # ******************************************************
  # The filepath of the certificate as it will be stored in the controller.
  # Type: string
  DeployedSSLCertificatePath: /etc/pki/tls/private/overcloud_endpoint.pem

  # *********************
  # End static parameters
  # *********************
resource_registry:
  OS::TripleO::NodeTLSData: /usr/share/openstack-tripleo-heat-templates/puppet/extraconfig/tls/tls-cert-inject.yaml

EOF
if [ "$OSPVER" -ge "12" ]; then
  cat <<EOF > templates/inject-trust-anchor-hiera-environment.yaml
# title: Inject SSL Trust Anchor on Overcloud Nodes
# description: |
#   When using an SSL certificate signed by a CA that is not in the default
#   list of CAs, this environment allows adding a custom CA certificate to
#   the overcloud nodes.
parameter_defaults:
  # Map containing the CA certs and information needed for deploying them.
  # Type: json
  CAMap:
    overcloud-cacert:
      content: |
`sed -n -e 's/^/         /' -e '/-----BEGIN CERTIFICATE-----/,$p' ca.crt.pem`
    # from /etc/pki/ca-trust/source/anchors/cm-local-ca.pem
    undercloud-cacert:
      content: |
`sed -n -e 's/^/         /' -e '/-----BEGIN CERTIFICATE-----/,$p' ../undercloud-cm-local-ca.pem`

EOF
echo -e "\nA enable-tls-environment.yaml and inject-trust-anchor-hiera-environment.yaml has been created in $PWD/templates. Copy them to ~/templates to use them:\n  cp -a $PWD/templates/* templates/"
else
  cat <<EOF > templates/inject-trust-anchor-environment.yaml
# title: Inject SSL Trust Anchor on Overcloud Nodes
# description: |
#   When using an SSL certificate signed by a CA that is not in the default
#   list of CAs, this environment allows adding a custom CA certificate to
#   the overcloud nodes.
parameter_defaults:
  # The content of a CA's SSL certificate file in PEM format. This is evaluated on the client side.
  # Mandatory. This parameter must be set by the user.
  # Type: string
  SSLRootCertificate: |
`sed -n -e 's/^/     /' -e '/-----BEGIN CERTIFICATE-----/,$p' ca.crt.pem`

resource_registry:
  OS::TripleO::NodeTLSCAData: /usr/share/openstack-tripleo-heat-templates/puppet/extraconfig/tls/ca-inject.yaml

EOF
echo -e "\nA enable-tls-environment.yaml and inject-trust-anchor-environment.yaml has been created in $PWD/templates. Copy them to ~/templates to use them:\n  cp -a $PWD/templates/* templates/"
fi

cd
