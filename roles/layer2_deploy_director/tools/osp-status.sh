#!/bin/bash
source ~/stackrc || exit 1
source ~/bin/osp-env.sh || exit 2
[ "$1" = "--all" ] && openstack service list
nova list
[ "$OSPVER" -le "13" ] && ironic node-list || openstack baremetal node list
#openstack baremetal introspection bulk status
openstack baremetal introspection list
openstack stack list
openstack overcloud profiles list
