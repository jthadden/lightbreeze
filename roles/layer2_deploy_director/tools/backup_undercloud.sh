#!/bin/bash
WHEN="`date +%Y%m%d-%H%m`"
sudo -i <<EOF
mysqldump --opt --all-databases > /root/undercloud-all-databases-${WHEN}.sql
tar --xattrs -czf undercloud-backup-${WHEN}.tar.gz /root/undercloud-all-databases-${WHEN}.sql /etc/my.cnf.d/galera.cnf /var/lib/glance/images /srv/node /home/stack /etc/pki/instack-certs/undercloud.pem /etc/pki/ca-trust/source/anchors/ca.crt.pem /home/stack/{certs,templates,bin}
EOF