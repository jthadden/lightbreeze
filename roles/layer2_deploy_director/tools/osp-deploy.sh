#!/bin/bash
help() {
cat <<EOF
Usage: $0 [-h]

This script deploys the overcloud. When called with "--update-plan-only" as \$1 it updates the plan.

EOF
exit 0
}

[ "$1" = "-h" ] && help

### PATCH to be sure an extraconfig is run
# should be done with crudini or the matteo approach
#echo " " >> /home/stack/templates/extraconfig/cinder-backends.sh 
####


source stackrc || exit 1
# name[0]:VMs[1]:CPUs[2]:MEM[3]:HDD[4]:NICs[5]
source /home/stack/bin/osp-env.sh || exit 1

#OSPVER="`grep openstack-nova-compute overcloud_images-environment.yaml | cut -d: -f3 | sort | uniq|cut -d. -f1`"

export ACTIONTAG="deploy-`date +%Y%m%d-%H:%M`"
[ "${ACTIONCMD}" ] || export ACTIONCMD="deploy --stack overcloud $1"
export TEMPLATEDIR="--templates /usr/share/openstack-tripleo-heat-templates"
# format: (1) no file means no network-isolation.yaml, (2) file or symlink means to use that
export NETISOL="`readlink /home/stack/templates/network-isolation*.yaml`"
[ "$NETISOL" = "" ] && unset NETISOL && echo "Attention: Having no network-isolation\*.yaml means deploying without any network segregation."

clear
#echo -e "$0 $@\n${ACTIONTAG}\n$ACTIONCMD\n$NODEUUIDS"
echo

# upload static files, if they exist
if cd ~/templates/files 2>/dev/null; then
  if [ ! -e ~/files.tar.gz ]; then
    echo "Uploading static files to deployment..."
    sudo chown -Rf root.root *
    sudo tar -cpvzf ~/files.tar.gz *
    sudo chown -Rf stack.stack *
    #swift list overcloud-artifacts|grep files.tar.gz
    upload-swift-artifacts -f ~/files.tar.gz
    echo
  fi
  cd ~
  echo
fi

# force extraconfigs to be run every time
# this should never be done this way
#for script in ~/templates/extraconfig/*; do
#  echo " " >> $script
#done

free -h
echo
df -h
echo
[ "$NETISOL" ] && echo "Using network isolation from $NETISOL." || echo "Using NO network isolation."
export NETISOL="-e $NETISOL"
echo

echo openstack --log-file osp-${ACTIONTAG}.out overcloud $ACTIONCMD \
    ${TEMPLATEDIR} \
    `[ -e /home/stack/templates/roles_data.yaml ] && echo "-r /home/stack/templates/roles_data.yaml"` \
    $NETISOL \
    `for n in ~/templates/*environment*.yaml; do echo -n "-e $(readlink -f "$n") "; done` \
    > osp-${ACTIONTAG}.out

echo "We will use the following deploy command:"
head -1  osp-${ACTIONTAG}.out
cat <<EOF

Check for enough memory, please: You should have at least 6GB ram and
20GB on /var and 10GB on / free. You must not bind mount to solve storage
issues on the director as this leads to cross device link errors during
deployment!
EOF
read -p "Are you shure to continue (ctrl-c to abort)? "

# starting with OSP10 the openstack overcloud deploy command can not run in background...
cat <<EOF

Watch Process
=============
1) Use the following in another window to check the progress of the stacks:
watch -n60 "tail osp-${ACTIONTAG}.*;echo -e '\n';free -h;echo -e '\n';openstack stack resource list -n5 overcloud | grep -v _COMPLETE"

2) Use the following in another window to check the nodes status:
watch -n30 "nova list;echo;ironic node-list;echo;iscsiadm -m session"
EOF

cat <<"EOF"

3) Starting with Deployment_Step1 of the deployment (as observed in first window) you can also follow the creation of container services via:
CTRLs="`openstack server list|grep "control"|sed -e "s/^.*ctlplane=\([^\ ]\+\).*$/\1/"|paste -s`"
CTRL1="`echo $CTRLs|awk '{print $1}'`"
watch -n30 "for CTRL in $CTRLs; do echo -e \"########## \$CTRL ##########\";ssh -q heat-admin@\$CTRL sudo docker ps -a|grep -v '(0)'|head -10;done;echo -e \"\n\";ssh -q heat-admin@$CTRL1 sudo pcs status|grep -v \"bundle-.\s\|docker-\"|tail -n +10"

EOF

yes "" | time openstack --log-file osp-${ACTIONTAG}.out overcloud $ACTIONCMD \
    ${TEMPLATEDIR} \
    `[ -e /home/stack/templates/roles_data.yaml ] && echo "-r /home/stack/templates/roles_data.yaml"` \
    $NETISOL \
    `for n in ~/templates/*environment*.yaml; do echo -n "-e $(readlink -f "$n") "; done` \
    2>&1 >> osp-${ACTIONTAG}.out
