# base definitions of environment
OSPVER="{{ OSPVER }}"
HYPERC="{{ hyperconverged|default("") }}"

# OSP params
EXTPOOLSTART="{{ float_pool_start }}"
EXTPOOLEND="{{ float_pool_end }}"
TIME="{{ ntp1 }}"
TIMEZONE="{{ timezone }}"		# set the timezone
MYDOMAIN="{{ dnsdomain }}"		# domain name
NFSSERVER="{{ director.ip }}"		# Pinned to the Director
ADDRPMS="{{ osp_additional_rpms|default('') }}"
