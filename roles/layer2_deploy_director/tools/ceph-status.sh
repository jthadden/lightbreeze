#!/bin/bash
help() {
cat <<EOF
Usage: $0 [-h|--pools|--osd-state]

This script checks the ceph installation with some status and can list the pools as well.

EOF
exit 0
}

[ "$1" = "-h" ] && help

source osp-env.sh || exit 1
source stackrc || exit 1
export ANSIBLE_HOST_KEY_CHECKING=False

grep '\[mon1\]' /etc/ansible/hosts &>/dev/null || osp-prep-ansible-inventory.sh

for COMMAND in "date" "ceph status" "ceph df" "ceph osd tree" "ceph osd pool stats"; do
  echo "# $COMMAND"
  ansible mon1 -b -m shell -a "/bin/bash -c \". ~/.bashrc && . ~/.bash_profile && $COMMAND\""
done

echo "# osd states"
[ "$1" = "--osd-state" ] && ansible computes -b -m shell -a 'echo -e "#########################################################\n################## `hostname` ##################\n#########################################################\n";systemctl status `basename /etc/systemd/system/ceph-osd.target.wants/ceph-osd@*`'
[ "$1" = "--pools" ] || exit
for POOL in images volumes vms; do
  echo "# Content of pool \"$POOL\""
  ansible mon1 -b -m shell -a "/bin/bash -c \". ~/.bashrc && . ~/.bash_profile && rbd -p $POOL ls -l\""
done
