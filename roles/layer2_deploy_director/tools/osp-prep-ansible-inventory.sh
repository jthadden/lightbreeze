#!/bin/bash
help() {
cat <<EOF
Usage: $0 [-h|--pools]

This script creates the ansible hosts file.

EOF
exit 0
}

[ "$1" = "-h" ] && help

source osp-env.sh || exit 1
source stackrc || exit 1
[ -e ansible-inventory.yaml ] && exit 0

cd

if [ "$OSPVER" -ge "13" ]; then
  /usr/bin/tripleo-ansible-inventory --stack overcloud --static-yaml-inventory ansible-inventory.yaml
else
  /usr/bin/tripleo-ansible-inventory --static-inventory ansible-inventory.yaml
fi

mkInventory() {
    cat <<EOF >>ansible-inventory.yaml

controllers:
  children:
    Controller: {}
  vars: {ansible_ssh_user: heat-admin}

controller1:
  hosts:
`grep -A2 ^Controller: ansible-inventory.yaml|tail -1` {}
  vars: {ansible_ssh_user: heat-admin}

computes:
  children:
    Compute: {}
  vars: {ansible_ssh_user: heat-admin}

mons:
  children:
    ceph_mon: {}
  vars: {ansible_ssh_user: heat-admin}

osds:
  children:
    ceph_osd: {}
  vars: {ansible_ssh_user: heat-admin}

EOF

  if [ "$HYPERC" != "" ]; then
    cat <<EOF >>ansible-inventory.yaml
mon1:
  hosts:
`grep -A2 ^Controller: ansible-inventory.yaml|tail -1` {}
  vars: {ansible_ssh_user: heat-admin}

EOF
  else
    cat <<EOF >>ansible-inventory.yaml
mon1:
  hosts:
`grep -A2 ^ceph_mon: ansible-inventory.yaml|tail -1` {}
  vars: {ansible_ssh_user: heat-admin}

EOF
  fi
}

mkInventory12() {
    cat <<EOF >>ansible-inventory.yaml

[controller:children]
Controller

[controller:vars]
ansible_ssh_user=heat-admin

[controllers:children]
Controller

[controllers:vars]
ansible_ssh_user=heat-admin

[controller1:children]
`grep -A2 "^\[Controller:children\]" ansible-inventory.yaml|tail -1`

[controller1:vars]
ansible_ssh_user=heat-admin

[compute:children]
Compute

[compute:vars]
ansible_ssh_user=heat-admin

[computes:children]
Compute

[computes:vars]
ansible_ssh_user=heat-admin

[mons:children]
ceph_mon

[mons:vars]
ansible_ssh_user=heat-admin

[osds:children]
ceph_osd

[osds:vars]
ansible_ssh_user=heat-admin

director ansible_host="{{ director.ip }}" ansible_ssh_user=stack

EOF

  if [ "$HYPERC" != "" ]; then
    cat <<EOF >>ansible-inventory.yaml
[mon1:children]
`grep -A2 "^\[Controller:children\]" ansible-inventory.yaml|tail -1`

[mon1:vars]
ansible_ssh_user=heat-admin

EOF
  else
    cat <<EOF >>ansible-inventory.yaml
[mon1:children]
`grep -A2 "^\[ceph_mon:children]" ansible-inventory.yaml|tail -1`

[mon1:vars]
ansible_ssh_user=heat-admin

EOF
  fi
}

if [ "$OSPVER" -ge "13" ]; then
  mkInventory
else
  mkInventory12
fi
[ -e /etc/ansible/hosts.org ] || sudo mv /etc/ansible/hosts /etc/ansible/hosts.org
sudo cp ansible-inventory.yaml /etc/ansible/hosts
