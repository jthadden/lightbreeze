- name: correct overcloudrc for ssl usage
  shell: sed -i -e "s/Certificate has no/Certificate for {{ controllervip.ip }} has no/" /home/stack/overcloudrc

- name: prepare an inventory
  shell: |
      cd /home/stack
      export PATH="$PATH:/home/stack/bin"
      osp-prep-ansible-inventory.sh

- name: get controllers ips
  shell: |
    export ANSIBLE_HOST_KEY_CHECKING=False
    IFACE="{% if ENV == 'lab' %}br-ex{% else %}vlan{{ networks.externalapi.vlan }}{% endif %}"
    ansible controllers --extra-var ansible_user=heat-admin -b -m shell -a "ip addr show $IFACE"|grep inet\ |awk '{print $2}'|grep -v {{ controllervip.ip }}|cut -d/ -f1
  register: controllers_ips
- debug: var=controllers_ips

- name: get computes ips
  shell: |
    export ANSIBLE_HOST_KEY_CHECKING=False
    IFACE="{{ networks.provisioning.nic }}"
    ansible computes --extra-var ansible_user=heat-admin -b -m shell -a "ip addr show $IFACE"|grep inet\ |awk '{print $2}'|grep -v {{ controllervip.ip }}|cut -d/ -f1
  register: computes_ips
- debug: var=computes_ips

- name: set controllers facts ansible_host and ansible_user
  set_fact:
    ansible_host: "{{ item.1 }}"
    ansible_user: heat-admin
  delegate_to: "{{ hostvars[item.0].inventory_hostname }}"
  delegate_facts: yes
  with_together: 
    - "{{ groups['controllers'] }}"
    - "{{ controllers_ips.stdout_lines }}"

- name: set computes facts ansible_host and ansible_user
  set_fact:
    ansible_host: "{{ item.1 }}"
    ansible_user: heat-admin
  delegate_to: "{{ hostvars[item.0].inventory_hostname }}"
  delegate_facts: yes
  with_together: 
    - "{{ groups['computes'] }}"
    - "{{ computes_ips.stdout_lines }}"

- debug: msg="{{ hostvars[item].inventory_hostname }} = {{ hostvars[item].ansible_host }}"
  with_items:
    - "{{ groups['controllers'] }}"
    - "{{ groups['computes'] }}"

- name: check for overcloud already prepared
  stat:
    path: "/home/{{ stack_user }}/{{ inventory_hostname }}_overcloud_prepared"
  register: overcloud_prepared

- block:

    - name: publish directors authorized_keys to controllers and computes (e.g. for designate installation)
      shell: |
          export ANSIBLE_HOST_KEY_CHECKING=False
          export KEY="`cat ~/.ssh/authorized_keys`"
          ansible controllers,computes --extra-var ansible_user=heat-admin -b -m shell -a "grep \"$KEY\" .ssh/authorized_keys || echo \"$KEY\" >> .ssh/authorized_keys"

    - name: link mysql credentials to /root (e.g. for designate and galera-status.sh)
      shell: |
          export ANSIBLE_HOST_KEY_CHECKING=False
          export KEY="`cat ~/.ssh/authorized_keys`"
          ansible controllers --extra-var ansible_user=heat-admin -b -m shell -a "[ -e /root/.my.cnf ] || ln -s /var/lib/config-data/mysql/root/.my.cnf /root"

    - name: correct ceph via director using ansible
      shell: |
          export ANSIBLE_HOST_KEY_CHECKING=False
          ansible all -i '{{ controllervip.ip }},' --extra-var ansible_user=heat-admin -b -m shell -a '/bin/bash -c ". ~/.bashrc && . ~/.bash_profile && for n in \`ceph osd pool ls\`; do [ \$n = 'metrics' ] || ceph osd pool application enable \$n rbd || true; done"'
      when: hyperconverged|default(false)

    # for pacemaker, see: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/high_availability_add-on_reference/#resourcemodify    
    - name: update pacemaker resource nova-evacuate for usage of shared_storage (as fencing-sharedstorage-environment.yml seems not to be picked up)
      shell: |
          export ANSIBLE_HOST_KEY_CHECKING=False
          [ -e ~/templates/compute-instanceha-environment.yaml ] && ansible all -i '{{ controllervip.ip }},' --extra-var ansible_user=heat-admin -b -m shell -a 'pcs resource update nova-evacuate no_shared_storage=false'
          /bin/true
      when: hyperconverged|default(false)

    - name: cleanup pacemaker after install
      shell: |
          export ANSIBLE_HOST_KEY_CHECKING=False
          ansible all -i '{{ controllervip.ip }},' --extra-var ansible_user=heat-admin -b -m shell -a 'pcs resource cleanup'

    - name: wait 30s for pcs to be sane again
      pause:
        seconds: 30
      
    - name: cleanup unhealthy docker containers after install
      shell: |
          export ANSIBLE_HOST_KEY_CHECKING=False
          ansible controllers --extra-var ansible_user=heat-admin -b -m shell -a "docker ps|grep unhealthy && docker restart \`docker ps|grep unhealthy|awk '{print \$1}'\` || /bin/true"

    - name: wait 30s for containers to be sane again
      pause:
        seconds: 30

    - name: set state "overcloud already prepared"
      file:
        path: "/home/{{ stack_user}}/{{ inventory_hostname }}_overcloud_prepared"
        state: touch

  when: mode=='create' and not overcloud_prepared.stat.exists
