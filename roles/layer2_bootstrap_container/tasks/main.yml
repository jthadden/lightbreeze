# when we exit this role we should have a machine with:
# - ip statically set on a reachable device
# - sshd enabled and running
# - passwd for root set

- name: check for "{{ inventory_hostname }}" exists
  shell: LANG=C lxc ls | grep " {{ inventory_hostname }} "
  register: cont_exists
  ignore_errors: yes
  delegate_to: lxdhost
- debug: var=cont_exists

- name: check for "{{ inventory_hostname }}" already running
  shell: LANG=C lxc ls | grep " {{ inventory_hostname }} " | grep " RUNNING "
  register: cont_running
  ignore_errors: yes
  delegate_to: lxdhost
- debug: var=cont_running

- name: start layer2 containers
  lxd_container:
    name: "{{ inventory_hostname }}"
    architecture: x86_64
    state: started
  delegate_to: lxdhost
  when: cont_exists.rc == 0 and cont_running.rc != 0

- name: wait 120s for ssh to appear
  wait_for:
    host: "{{ ansible_host }}"
    port: 22
    delay: 0
    timeout: 120
  delegate_to: layer1host
  when: cont_exists.rc == 0 and cont_running.rc != 0

- name: is the node already bootstrapped?
  stat:
    path: "/root/{{ inventory_hostname }}_bootstrapped"
  register: host_bootstrapped
  when: cont_running.rc == 0
- debug: var=host_bootstrapped

- block:
    - name: "kill a dhclient that can interfere and remove ifcfg-{{ nic1 }}"
      raw: 'ifdown {{ nic1 }};rm /etc/sysconfig/network-scripts/ifcfg-{{ nic1 }};killall dhclient;ifconfig {{ nic1 }} down'
      ignore_errors: yes
    - name: "config {{ nic1 }}"
      raw: 'ip addr add "{{ initip }}/{{ netmask }}" dev {{ nic1 }}'
      ignore_errors: yes
    - name: "setting {{ nic1 }} up"
      raw: 'ip link set {{ nic1 }} up'
    - name: add route
      raw: "ip route add default via {{ gateway }}"
      ignore_errors: yes
    - name: add dns
      raw: 'echo -e "nameserver {{ dns1 }}\nnameserver {{ dns2 }}" > /etc/resolv.conf'
    - name: check for python is installed in rhel7 container
      raw: rpm -q python
      register: python_install_check
      failed_when: python_install_check.rc not in [0, 1]
      changed_when: false
      when: ostype == 'rhel7'
    - name: check for python2 is installed in fedora container
      raw: rpm -q python2
      register: python2_install_check
      failed_when: python2_install_check.rc not in [0, 1]
      changed_when: false
      when: ostype == 'fedora'
    - name: pre-install python in container
      raw: "{{ package_manager }} -y install python"
      when: (ostype=='rhel7' and python_install_check.rc == 1) or (ostype=='fedora' and python2_install_check.rc == 1)
      ignore_errors: yes

    ###################################################################################################################
    # repo und subs part
    # we only need basic repos here, so we just use the "ostype" repo and skip all other product related repos

    # 0. make sure there are no other repos available, if we have a repo_overwrite
    - name: disable all repos, if repo overwrite (with a very weired manual method)
      #shell: mkdir -p /etc/yum.repos.d/.old;for n in /etc/yum.repos.d/*; do [ -s "$n" ] && cp -a "$n" /etc/yum.repos.d/.old;> "$n";done
      shell: |
          [ -z "`ls -A /etc/yum.repos.d`" ] || ( mkdir -p /etc/yum.repos.d/.old;for n in /etc/yum.repos.d/*; do [ -s "$n" ] && cp -a --backup=numbered "$n" /etc/yum.repos.d/.old;> "$n";done )
          #sed -i -e "s/enabled=1/enabled=0/" /etc/yum.repos.d/*;sed -i -e "s/^\(\[.*\]\)/\1\nenabled=0/" /etc/yum.repos.d/*
      when:
        - (product is not defined or product != 'mirror')
        - repo_overwrite is defined
        - repo_overwrite[ostype] is defined
      ignore_errors: yes
 
    # 1. we have a repo_overwrite that we want to use
    - name: install base repo overwrites
      block:
        - name: "install repo files for {{ ostype }}"
          get_url:
            url: "{{ item }}"
            dest: /etc/yum.repos.d/
            mode: 0644
            force: yes
          with_items: "{{ repo_overwrite[ostype] }}"
        
        - name: temporarily fetch the repo template
          fetch:
            src:  "/etc/yum.repos.d/{{ item|basename }}"
            dest: "/tmp/{{ item|basename }}-{{ inventory_hostname }}.j2"
            flat: yes
          with_items: "{{ repo_overwrite[ostype] }}"
          
        - name: template temporary repo template
          template:
            src:  "/tmp/{{ item|basename }}-{{ inventory_hostname }}.j2"
            dest: "/etc/yum.repos.d/{{ item|basename }}"
          with_items: "{{ repo_overwrite[ostype] }}"
        
        - name: clean up temporary repo template file
          file:
            path:  "/tmp/{{ item|basename }}-{{ inventory_hostname }}.j2"
            state: absent
          with_items: "{{ repo_overwrite[ostype] }}"
      when:
        - (product is not defined or product != 'mirror')
        - repo_overwrite is defined
        - repo_overwrite[ostype] is defined

    # 2. we have a sub
    - name: subscribe
      redhat_subscription:
        state: present
        consumer_name: "{{ inventory_hostname|lower }}.{{ dnsdomain|lower }}"
        username: "{{ subscription_user }}"
        password: "{{ subscription_pw }}"
        pool_ids: "{{ subscription_pool }}"
        auto_attach: true
        force_register: true
      when:
        - ostype == 'rhel7'
        - ((product is defined and product == 'mirror') or repo_overwrite is not defined or repo_overwrite[ostype] is not defined)
    - name: disable all repos
      shell: subscription-manager repos --disable \* >/dev/null
      when:
        - ostype == 'rhel7'
        - ((product is defined and product == 'mirror') or repo_overwrite is not defined or repo_overwrite[product] is not defined)
    - name: enable base repos
      shell: subscription-manager repos --enable rhel-7-server-rpms --enable rhel-7-server-extras-rpms --enable rhel-7-server-optional-rpms
      when:
        - ostype == 'rhel7'
        - ((product is defined and product == 'mirror') or repo_overwrite is not defined or repo_overwrite[product] is not defined)

    # 3. we have some repos
    - name: install repo_files
      shell: "curl {{ item }} -o /etc/yum.repos.d/`basename {{ item }}`"
      #shell: "yum install -y {{ item }} || yum reinstall -y {{ item }}"
      when:
        - ostype != 'rhel7'
        - ((product is defined and product == 'mirror') or repo_overwrite is not defined or repo_overwrite[ostype] is not defined)
        - (repo_files is defined and repo_files[ostype] is defined)
      with_items: "{{ repo_files[ostype] }}"

    # 4. (re-)install repo_packages if we don't have a repo_overwrite
    - name: install repo_packages
      shell: "yum install -y {{ item }} || yum reinstall -y {{ item }}"
      #package:
      #  name: "{{ item }}"
      #  state: installed
      #  use: "{{ package_manager }}"
      when:
        - ((product is defined and product == 'mirror') or repo_overwrite is not defined or repo_overwrite[ostype] is not defined)
        - (repo_packages is defined and repo_packages[ostype] is defined)
      with_items: "{{ repo_packages[ostype] }}"

    # 6. install additional local repo files
    - name: test for additional repo file
      local_action: stat path=files/{{ item }}-additional.repo
      register: "add_repo"
      with_items:
        - "{{ ostype }}"
        
    - debug: var=add_repo

    - name: add additional repo if exists
      copy:
        src: ../../../files/{{ item.item }}-additional.repo
        dest: /etc/yum.repos.d/{{ item.item }}-additional.repo
        owner: root
        group: root
        mode: u=rwx,g=r,o=r
      when:
        - item.stat.exists == true
        - ((product is defined and product == 'mirror') or repo_overwrite is not defined or repo_overwrite[ostype] is not defined)
      with_items: "{{ add_repo.results }}"


    - name: "as we don't explicitely enable something here, we disable some repos that might come into our way"
      command: "yum-config-manager --disable {{ item }}"
      ignore_errors: yes
      with_items:
        - rhel-7-server-htb-rpms
      when: ostype=='rhel7'
    ###################################################################################################################



    - name: install python in container
      raw: "{{ package_manager }} -y install python"
      when: (ostype=='rhel7' and python_install_check.rc == 1) or (ostype=='fedora' and python2_install_check.rc == 1)
      ignore_errors: yes
    - name: install NetworkManager
      package:
        name: "{{ item }}"
        state: installed
        use: "{{ package_manager }}"
      with_items:
        - dbus-python
        - NetworkManager
        - libsemanage-python
        - policycoreutils-python
    - name: install NetworkManager addons for fedora
      package:
        name: "{{ item }}"
        state: installed
        use: "{{ package_manager }}"
      with_items:
        - NetworkManager-glib
        - python-gobject
        - nm-connection-editor
      when: ostype=='fedora'
      
    #- name: stop sysv network
    #  service:
    #    name: network
    #    enabled: no
    #    state: stopped
    #- shell: rm -rf /etc/sysconfig/network-scripts/ifcfg-eth*
    - name: "definitely remove cloud-init"
      package:
        name: "{{ item }}"
        state: absent
        use: "{{ package_manager }}"
      with_items:
        - cloud-init

    - name: start NetworkManager
      service:
        name: NetworkManager
        enabled: no
        state: started
    - name: give 5s grace time
      pause: seconds=5
    - name: "re-create {{ nic1 }}"
      command: "nmcli con add con-name {{ nic1 }} ifname {{ nic1 }} type ethernet ip4 {{ initip }}/{{ netmask }} gw4 {{ gateway }} ipv4.dns '{{ dns1 }} {{ dns2 }}'"
      #nmcli:
        #conn_name: "{{ nic1 }}"
        #ifname: "{{ nic1 }}"
        #type: ethernet
        ##autoconnect: yes
        #ip4: "{{ initip }}/{{ netmask }}"
        #gw4: "{{ gateway }}"
        #dns4: "{{ dns1 }} {{ dns2 }}"
        #state: present
    - name: "ensure {{ nic1 }} is up at boot"
      lineinfile:
        dest: "/etc/sysconfig/network-scripts/ifcfg-{{ nic1 }}"
        regexp: "^ONBOOT=.*"
        line: "ONBOOT=yes"
    - lineinfile:
        dest: "/etc/sysconfig/network-scripts/ifcfg-{{ nic1 }}"
        regexp: "^NM_CONTROLLED="
        line: "NM_CONTROLLED=no"
    - lineinfile:
        dest: "/etc/sysconfig/network-scripts/ifcfg-{{ nic1 }}"
        regexp: "^#/sbin/ifconfig {{ nic1 }} down"
        line: "#/sbin/ifconfig {{ nic1 }} down"
        insertbefore: BOF
    - name: install sshd and some very basic tools needed later
      package:
        name: "{{ item }}"
        state: installed
        use: "{{ package_manager }}"
      with_items:
        - openssh-server
        - iptables-services
        - "{% if ostype=='fedora' %} iptables-utils {% else %} iptables {% endif %}"
        - file
        - less
        - net-tools
        - yum-utils
    - name: enable and start sshd
      service:
        name: sshd
        enabled: yes
        state: started
    - name: install rc.local
      copy:
        src: ../files/rc.local-container
        dest: /etc/rc.local
        owner: root
        group: root
        mode: u=rwx,g=rx,o=rx
    - name: exec rc.local
      shell: /etc/rc.local
    - name: set root password
      shell: echo "{{ container_pw }}"|passwd root --stdin
    - name: set /root/.ssh/authorized_keys
      authorized_key:
        user: root
        state: present
        key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
    - name: touch a lock file
      file:
        path: "/root/{{ inventory_hostname }}_bootstrapped"
        state: touch

  when: mode=='create' and not host_bootstrapped.stat.exists

- block:
    - name: unsubscribe
      redhat_subscription:
        state: absent
        consumer_name: "{{ inventory_hostname|lower }}.{{ dnsdomain|lower }}"
        username: "{{ subscription_user }}"
        password: "{{ subscription_pw }}"
        auto_attach: true
        force_register: true
      when: ostype == 'rhel7'
      # and (repo_overwrite is not defined or repo_overwrite[ostype] is not defined)
  when:
    - mode=='destroy'
    - cont_exists.rc == 0
    - product is not defined or (product is defined and product!='mirror')
