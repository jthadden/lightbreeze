#!/bin/bash
# This scipt configures nova as backends on computes.

# not working like this with containers: crudini --set /etc/nova/nova.conf DEFAULT injected_network_template /usr/local/etc/interfaces.template

#################################################################
# on computes only
#################################################################
if [[ `hostname` = *"compute"* ]]; then
  grep 192.168.0.70:/srv/node/nova-nfs /etc/fstab || echo "192.168.0.70:/srv/node/nova-nfs   /var/lib/nova/instances nfs _netdev,defaults 0 0" >> /etc/fstab
  #docker stop nova_libvirt nova_compute
  mount -a
  # deprecated: semodule -i /root/my-virtlogd.pp
  # deprecated: semodule -i /root/my-sshd.pp
  setsebool -P virt_use_nfs 1
  #docker start nova_libvirt nova_compute
fi

exit 0

