#!/bin/bash
exec &> /var/log/firstboot-setup-`date +%Y%m%d%S`.log

permit_ssh()
{
  # Permit root login over SSH
  echo "correcting sshd..."
  (
  while true; do
    OLD="`ls -l /etc/ssh/sshd_config`"
    grep "^PasswordAuthentication no" /etc/ssh/sshd_config          && sed -i 's/^PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    grep "^PasswordAuthentication" /etc/ssh/sshd_config             || echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
    grep "^ChallengeResponseAuthentication no" /etc/ssh/sshd_config && sed -i 's/^ChallengeResponseAuthentication.*/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
    grep "^ChallengeResponseAuthentication" /etc/ssh/sshd_config    || echo "ChallengeResponseAuthentication yes" >> /etc/ssh/sshd_config
    grep "^UseDNS yes" /etc/ssh/sshd_config                         && sed -i 's/^UseDNS.*/UseDNS no/g' /etc/ssh/sshd_config
    grep "^UseDNS" /etc/ssh/sshd_config                             || echo "UseDNS no" >> /etc/ssh/sshd_config
    NEW="`ls -l /etc/ssh/sshd_config`"
    #chattr +i /etc/ssh/sshd_config
    [ "$OLD" = "$NEW" ] || systemctl restart sshd
    sleep 10s
  done
  ) &
}

set_rootpw()
{
  # Update the root password to something we know
  echo "{{ vm_pw }}" | sudo passwd root --stdin
}

register()
{
  subscription-manager register --username '{{ subscription_user }}' --password '{{ subscription_pw }}'
  subscription-manager attach --pool="{{ subscription_pool }}"
  subscription-manager repos --disable=*
  subscription-manager repos --enable rhel-7-server-rpms --enable rhel-7-server-optional-rpms --enable rhel-7-server-extras-rpms --enable rhel-7-server-rh-common-rpms --enable rhel-7-server-supplementary-rpms --enable rhel-ha-for-rhel-7-server-rpms --enable rhel-7-server-openstack-13-rpms --enable rhel-7-server-openstack-13-devtools-rpms --enable rhel-7-server-openstack-13-optools-rpms --enable rhel-7-server-rhceph-3-osd-rpms --enable rhel-7-server-rhceph-3-mon-rpms --enable rhel-7-server-rhceph-3-tools-rpms
}

nova_nfs()
{
  if [[ `hostname` = *"compute"* ]]; then
    setsebool -P virt_use_nfs 1
    grep 192.168.0.70:/srv/node/nova-nfs /etc/fstab || echo "192.168.0.70:/srv/node/nova-nfs   /var/lib/nova/instances nfs _netdev,defaults 0 0" >> /etc/fstab
    mount -a
  fi
}

rp_filter_accept()
{
  # configure rp filter to accept packets on all links
  echo 2 > /proc/sys/net/ipv4/conf/all/rp_filter
}

replace_partx()
{
  # if partx is still broken we replace it by a script that simulates it
  if [ ! -e /usr/sbin/partx.org ]; then
    mv /usr/sbin/partx /usr/sbin/partx.org
    cat <<"END" > /usr/sbin/partx
#!/bin/bash
[[ $1 = *"dev"* ]] && DEV="$1" || DEV="$2"
/usr/sbin/partx.org -u ${DEV}
echo "`date` partx $@" >> /tmp/partx.out
exit 0
END
    chmod 755 /usr/sbin/partx
    #ln -s /bin/true /usr/sbin/partx
  fi
}

wipe_disks()
{
  # This script wipes all disks except for the root disk to make sure there's nothing
  # left from a previous install and to have GPT labels in place.
  export LVM_SUPPRESS_FD_WARNINGS=true
  echo -e "\nPreparing disks for local storage usage...\n================================================="
  echo "Number of disks detected: $(lsblk -no NAME,TYPE,MOUNTPOINT | grep "disk" | awk '{print $1}' | wc -l)"
  echo "Number of mpath devices: $(ls /dev/mapper/mpath[a-z] 2>/dev/null | wc -l)"
  multipath -ll
  DISKDEVS=""
  vgchange -an 2>/dev/null
  for VG in `vgs --noheadings --rows|head -1`; do
    vgremove -f $VG
  done
  cd /dev
  for DEVICE in `lsblk -no NAME,TYPE,MOUNTPOINT | grep "disk" | awk '{print $1}'` mapper/mpath[a-z]; do
    ROOTFOUND=0
    echo "Checking /dev/$DEVICE..."
    [ -e /dev/$DEVICE ] || continue
    echo "Number of partitions on /dev/$DEVICE: $(expr $(lsblk -n /dev/$DEVICE | awk '{print $7}' | wc -l) - 1)"
    for MOUNTS in `lsblk -n /dev/$DEVICE | awk '{print $7}'`; do
      if [ "$MOUNTS" = "/" ]; then
        ROOTFOUND=1
      fi
    done
    if [ $ROOTFOUND = 0 ]; then
      echo "Root not found in /dev/${DEVICE}"
      # if this device is part of an mpath we skip it
      lsblk -n /dev/$DEVICE 2>/dev/null | paste -s | grep disk | grep mpath && echo "/dev/${DEVICE} is an mpath device... skipping." && continue
      echo "Wiping disk /dev/${DEVICE}"
      partx -d /dev/${DEVICE}
      sgdisk -Z /dev/${DEVICE}
      sgdisk -g /dev/${DEVICE}
      partx -a /dev/${DEVICE}
      partx -u /dev/${DEVICE}
      DISKDEVS="${DISKDEVS} ${DEVICE}"
      lsblk -no SIZE -db /dev/${DEVICE}
      sync
    else
      echo "Root found in /dev/${DEVICE}... skipping."
    fi
  done
}

prep_cheph_disks()
{
  if [[ `hostname` = *"ceph"* ]]; then
    wipe_disks
  fi
}

permit_ssh
set_rootpw
#register
#nova_nfs
#rp_filter_accept
#replace_partx
# use wipe_disks instead of prep_cheph_disks for hyperconverged setups
#prep_cheph_disks
wipe_disks

exit 0

