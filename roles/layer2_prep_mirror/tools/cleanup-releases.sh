#!/bin/bash

rm -f pre* post* out*
yum -y remove `rpm -qa|grep release|grep -v redhat-release`
#yum -y remove ovirt-release42
rm -f /etc/yum.repos.d/*.rpmsave
rm -f /etc/yum.repos.d/{ovirt-4.2.repo,delorean-deps.repo}
rm -f fakeprovide-centos-release-*.noarch.rpm
rm -f *-mirror_mirror_prepared
grep releasever /etc/yum.repos.d/*|grep -v redhat
