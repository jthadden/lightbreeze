#!/bin/bash
# this comes from: https://gist.github.com/carbonin/a25b84efca2e6b3c3f91b673821a22c8
# and was extended to remove the VG as well

read -p "Attention: pressing RETURN will remove your complete database, disks, lvs and pgs. Press RETURN to continue..."

# Stop and disable services
systemctl stop evmserverd
systemctl disable evmserverd

systemctl stop $APPLIANCE_PG_SERVICE
systemctl disable $APPLIANCE_PG_SERVICE
> $APPLIANCE_PG_DATA/pg_log/postgresql.log

umount /dev/mapper/vg_pg-lv_pg /dev/vg_pg/lv_pg
vgremove -y vg_pg
sync
mv -f /etc/fstab /etc/fstab.last
grep -v "/dev/mapper/vg_pg-lv_pg\|/dev/vg_pg/lv_pg" /etc/fstab.last > /etc/fstab

umount /dev/mapper/vg_miq_logs-lv_miq_logs /dev/vg_miq_logs/lv_miq_logs
vgremove -y vg_miq_logs
sync
mv -f /etc/fstab /etc/fstab.last
grep -v "/dev/mapper/vg_miq_logs-lv_miq_logs\|/dev/vg_miq_logs/lv_miq_logs" /etc/fstab.last > /etc/fstab

disk="`mount|grep /var/www/miq_tmp|awk '{print $1}'|tail -1`"
umount /var/www/miq_tmp /dev/vdd1
( [ "$disk" = "/dev/vda3" ] || [ "$disk" = "" ] ) && disk="/dev/vdd1"
[ -b $disk ] && dd if=/dev/zero of=$disk bs=512 count=8 && sync && sleep 5s && sync
for n in a b c d e; do
  kpartx -au /dev/vd$n
done
mv -f /etc/fstab /etc/fstab.last
grep -v "/dev/vdd1" /etc/fstab.last > /etc/fstab
pvremove /dev/mapper/vdc1 /dev/mapper/vdb1

# Remove auto-generated files
pushd /var/www/miq/vmdb
  rm -f REGION GUID certs/* config/database.yml
popd

# Remove database
pushd $APPLIANCE_PG_DATA
  rm -rf ./*
popd

echo "You should remove the server now..."
