# Attention! This requires that
# - the overcloud has already been deployed (for sure)
# - the ansible users ssh-key has been published to the computes (e.g. directors authorized_keys published to computes)
# - the ansible_host fact of the computes has been set to correct values
# All of this has already been provided by the layer2_prep_overcloud role.

- name: add nfs to clients if we have an nfs server and not using ceph
  block:

    - name: check 10s for clients running
      wait_for:
        host: "{{ ansible_host }}"
        port: 22
        delay: 0
        timeout: 10
      delegate_to: "{{ director.ip }}"
      #delegate_to: layer1host
    
    - name: check for nfs client already setup
      stat:
        path: "/root/{{ inventory_hostname }}_nfs_client_prepared"
      when: mode=='create'
      register: nfs_client_prepared
    
    - block:
    
        - name: stop nova-compute
          service:
            name: "{{ item }}"
            state: stopped
          with_items:
            - openstack-nova-compute
          when: OSPVER<=11
        
        - name: add mount point for nova
          mount:
            src: "{{ hostvars[customer+'-nfs'].ansible_host }}:/srv/nova-nfs"
            path: /var/lib/nova/instances
            fstype: nfs
            opts: _netdev,defaults
            state: mounted
    
        - name: selinux should allow libvirt to use nfs (in case of lxc on lxdhost)
          seboolean:
            name: virt_use_nfs
            persistent: yes
            state: yes
          delegate_to: lxdhost
          when: virt_technology | default('') | regex_search('lxc')
    
        - name: selinux should allow libvirt to use nfs (in an lxc-container this will just do nothing)
          seboolean:
            name: virt_use_nfs
            persistent: yes
            state: yes
          when: not (virt_technology | default('') | regex_search('lxc'))
    
        - name: as we have frequent selinux errors on nfs shares, we add a default policy for that
          synchronize:
            rsync_opts:
              - "-e ssh {{ ansible_ssh_common_args }}"
            src: roles/layer2_add_nfs_to_computes/files
            dest: /root
            owner: no
            group: no
            perms: no
    
        - name: "add the new selinux rules to the machine (don't need this for containers as long as we have permissive running there)"
          shell: semodule -i /root/files/myNFS.pp
        
        - name: start nova-compute
          service:
            name: "{{ item }}"
            state: started
          with_items:
            - openstack-nova-compute
          when: OSPVER<=11
        
        - name: restart nova-compute
          shell: |
            for container in `docker ps|grep nova|awk '{print $1}'`; do
              docker restart $container
            done
          when: OSPVER>=12
    
        - name: touch a lock file
          file:
            path: "/root/{{ inventory_hostname }}_nfs_client_prepared"
            state: touch

      when: mode=='create' and not nfs_client_prepared.stat.exists

  when: (groups['nfs']|length) > 0 and not hyperconverged
