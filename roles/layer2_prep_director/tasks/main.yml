- name: check for tripleo already prepared
  stat:
    path: "/root/{{ inventory_hostname }}_tripleo_prepared"
  register: tripleo_prepared

- block:

    - name: add tools
      yum:
        name: "{{ item }}"
        state: latest
      with_items:
        - virt-v2v
        - openstack-utils
        - openldap-clients
    - name: install some optional software (that might fail installation because of not available repos)
      yum:
        name: "{{ item }}"
        #disable_gpg_check: yes
        enablerepo: epel
        state: installed
      with_items:
        - htop
      ignore_errors: yes

    - name: add groups if required
      group:
        name: "{{ item.0 }}"
        gid: "{{ item.1 }}"
        system: yes
      with_together:
        - [ 'glance', 'nova', 'cinder']
        - [ 161, 162, 165 ]

    - name: add users if required
      user:
        name: "{{ item.0 }}"
        comment: "{{ item.1 }}"
        uid: "{{ item.2 }}"
        group: "{{ item.3 }}"
        home: "{{ item.4 }}"
        system: yes
        create_home: no
        shell: /sbin/nologin
      with_together:
        - [ 'glance', 'nova', 'cinder']
        - [ 'OpenStack Glance Daemons', 'OpenStack Nova Daemons', 'OpenStack Cinder Daemons' ]
        - [ 161, 162, 165 ]
        - [ 161, 162, 165 ]
        - [ '/var/lib/glance', '/var/lib/nova', '/var/lib/cinder' ]

    - name: add user "{{ stack_user }}"
      user:
        name: "{{ stack_user }}"
        state: present
        append: true
    - shell: chage -W -1 -m -1 -M -1 "{{ stack_user }}"

    - name: set authorized key for user copying it from current user
      authorized_key:
        user: "{{ stack_user }}"
        state: present
        key: "{{ lookup('file', lookup('env','HOME') + '/.ssh/id_rsa.pub') }}"
    - name: allow user root access via sudo
      lineinfile:
        path: "/etc/sudoers.d/{{ stack_user }}"
        state: present
        create: yes
        regexp: '^{{ stack_user }} ALL='
        line: '{{ stack_user }} ALL=(root) NOPASSWD:ALL'
    - shell: "chmod 0440 /etc/sudoers.d/{{ stack_user }}"

    - name: install time server for provisioning network
      yum:
        name: "{{ item }}"
        state: installed
      with_items:
        - chrony
        - ntpdate

    - name: configure time server for provisioning network
      template:
        src: ../templates/chrony.conf
        dest: /etc/chrony.conf
        owner: root
        group: root
        mode: 0644

    #iptables -I INPUT -p udp --dport 123 -j ACCEPT
    - name: accept ntp port
      iptables:
        action: insert
        chain: INPUT
        protocol: udp
        destination_port: 123
        jump: ACCEPT
        comment: "allow ntp connections"

    #- name: make the rule stateful
    #  shell: |
    #      if [ -e /etc/sysconfig/iptables ]; then
    #        grep "INPUT -p udp --dport 123 -j ACCEPT" /etc/sysconfig/iptables || sed -i -e "s/^\(-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT\)$/-A INPUT -p udp --dport 123 -j ACCEPT\n\1/" /etc/sysconfig/iptables
    #      fi

    #iptables -I INPUT -i br-ctlplane -s 10.0.0.0/24 -d 10.0.0.10/32 -p tcp --dport 5672 -j ACCEPT
    - name: accept amqp access from provisioning network
      iptables:
        action: insert
        chain: INPUT
        protocol: tcp
        destination_port: 5672
        in_interface: "br-ctlplane"
        source: "{{ director.prov_cidr }}"
        destination: "{{ director.prov_cidr }}"
        jump: ACCEPT
        comment: "allow amqp connections from provisioning network"

    #- name: make the rule stateful
    #  shell: |
    #      if [ -e /etc/sysconfig/iptables ]; then
    #        grep "INPUT -i br-ctlplane -s {{ director.prov_cidr }} -d {{ director.prov_cidr }} -p tcp --dport 5672 -j ACCEPT" /etc/sysconfig/iptables || sed -i -e "s/^\(-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT\)$/-A INPUT -i br-ctlplane -s {{ director.prov_cidr }} -d {{ director.prov_cidr }} -p tcp --dport 5672 -j ACCEPT\n\1/" /etc/sysconfig/iptables
    #      fi

    - name: make the rules stateful
      shell: |
          #[ -e /etc/sysconfig/iptables ] || 
          iptables-save > /etc/sysconfig/iptables

    - name: enable chronyd
      service:
        name: chronyd
        enabled: yes

    - name: disable and stop ntpd
      service:
        name: ntpd
        enabled: no
        state: stopped
      ignore_errors: yes

    - name: restart services
      service:
        name: "{{ item }}"
        state: restarted
      with_items:
        - chronyd

    - name: check for chrony sources
      shell: sleep 5s && chronyc sources -v

    - debug: msg="### Step 2 - Install tripleo Installer"
    - name: install tripleo installer (might take some time, because of many deps)
      yum:
        name: "{{ item }}"
        state: latest
      with_items:
        - python-tripleoclient

    - name: for hyperconverged install ceph-ansible
      yum:
        name: "{{ item }}"
        state: latest
      when: hyperconverged|default(false)
      with_items:
        - ceph-ansible

    #rpm -q python-tripleoclient-0.3.4-5.el7ost
    #if [ "$?" = "0" ]; then
    #  echo "Because of https://bugzilla.redhat.com/show_bug.cgi?id=1347063 and https://access.redhat.com/solutions/2446961 we have to downgrade python-tripleoclient..."
    #  yum downgrade python-tripleoclient-0.3.4-4.el7ost.noarch
    #fi

    ## Bug 1343649 - Use of --timeout argument within ironic, breaks backwards compatibility with earlier versions of IPXE.
    ## https://bugzilla.redhat.com/show_bug.cgi?id=1343649#c27
    #sed -i 's,.*pxe/ipxe_timeout.*,,' /usr/share/instack-undercloud/puppet-stack-config/puppet-stack-config.pp
    #sed -i 's,.*ipxe_timeout.*,,' /usr/share/instack-undercloud/puppet-stack-config/puppet-stack-config.yaml.template

    ## Another unknown Bug:
    #grep "module ironic-ipxe" /usr/share/instack-undercloud/ipxe/selinux/ipxe.te && sed -i -e "s/\/ipxe.mod/\/ironic-ipxe.mod/" /usr/share/instack-undercloud/ipxe/post-install.d/86-selinux
    #for mod in mariadb rabbitmq; do
    #  grep "tripleo_selinux_$mod" /usr/share/tripleo-image-elements/selinux/custom-policies/tripleo-selinux-$mod.te && sed -i -e "s/tripleo_selinux_$mod/tripleo-selinux-$mod/" /usr/share/tripleo-image-elements/selinux/custom-policies/tripleo-selinux-$mod.te
    #done

    ### and another one for OSP10:
    #{ while ! ls /etc/puppet/modules/ironic/templates/inspector_ipxe.erb 2>/dev/null; do sleep 0.1s; done; sed -i -e "s/@ipxe_timeout_real/10/g" /etc/puppet/modules/ironic/templates/inspector_ipxe.erb; } &

    - name: in an lxc container we have to prepare several things
      block:
        - name: install additional software to be pre-patched (e.g. by rc.local)
          yum:
            name: "{{ item }}"
            #disable_gpg_check: yes
            state: installed
          with_items:
            - python-netaddr
            - audit
        - name: pre-install to be patched packages
          yum:
            name: "{{ item }}"
            state: latest
            #use: "{{ package_manager }}"
          with_items:
            - python-rtslib
        - name: patch iSCSI target
          command: sed -i -e "s/modprobe(/#modprobe(/g" /usr/lib/python2.7/site-packages/rtslib/root.py
        - command: sed -i -e "s/modprobe(/#modprobe(/g" /usr/lib/python2.7/site-packages/rtslib_fb/root.py
      when: virt_technology | regex_search('lxc')
    
    #- name: open firewall
    #  shell: |
    #      firewall-cmd --permanent --add-port={80/tcp,443/tcp,6080/tcp,22/tcp}
    #      firewall-cmd --reload

    - name: set state "tripleo prepared"
      file:
        path: "/root/{{ inventory_hostname }}_tripleo_prepared"
        state: touch

  when: mode=='create' and not tripleo_prepared.stat.exists
