# LightBreeze

A lightweight infrastructure installation and testing environment for
OpenSource projects and their related Red Hat products.

## Disclaimer

This git repository only contains personal content, views and opinions
and is not affiliated with Red Hat in any way. Furthermore this is
totally unsupported work. It is not meant to be working or solve anything
you are facing or think or might have as a problem in your life.

**Attention!** This project is not for deploying production environments.

## Configuration files used ##

### [config_infrastructure.yml](config_infrastructure.yml) ###

### [config-mirror.yml](config-mirror.yml) ###

### [config-IaC-rhosp-lab.yml](config-IaC-rhosp-lab.yml) ###

