#!/bin/bash

help()
{
  cat <<EOF
Usage: $0 [CUSTOMER [TAG [INFRA [CONFIG...]]]]

Parameters are used in the following way:
-i hosts\${CUSTOMER}${TAG} -e @config/config\${CUSTOMER}\${TAG}.yml -e @config/config_infrastructure\${INFRA}.yml -e @config/${CONFIG} ...

Defaults to:
$0 "-default" "" "" "config-mirror.yml"

Example for a minimal deployment:
$0 "-default" "-minimal"

EOF

exit 0
}

[ "$1" = "-h" ] || [ "$1" = "--help" ] && help

CUSTOMER="$1"
[ "$CUSTOMER" = "" ] && CUSTOMER="-default"
export CUSTOMER
shift

TAG="$1"
export TAG
shift

INFRA="$1"
export INFRA
shift

for c in $@; do
  CONFIG="${CONFIG} -e @config/$c"
done

echo
echo "Using: $0 \"${CUSTOMER}\" \"${TAG}\" \"${INFRA}\" \"${CONFIG}\""
echo

echo "###########################################"
echo "# I) prepare the environment"
echo "#      (Layer 0 & 1 w/o the VMs/Containers)"
echo "###########################################"
echo       "command: ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags layer0,layer1 --skip-tags starts_layer2 create.yml"
echo
read -p "Do you want to prepare the environment (y/N)? " answ
[ "$answ" = "y" ] && ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags layer0,layer1 --skip-tags starts_layer2 create.yml
echo
echo "###########################################"
echo "# II) destroy all nodes"
echo "###########################################"
echo       "command: ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} destroy.yml"
echo
read -p "Do you want to delete the nodes (y/N)? " answ
[ "$answ" = "y" ] && ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} destroy.yml
echo
echo "###########################################"
echo "# III) create the mirror"
echo "###########################################"
echo         "command: ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --extra-vars '{"create_mirror":true}' --limit layer1,lxc${CUSTOMER}-mirror,kvm${CUSTOMER}-mirror,${CUSTOMER:1}-mirror --tags starts_layer2,bootstrap,prep_hosts,mirror --skip-tags upload_kvmimages create.yml"
echo
read -p "Do you want to create the mirror (y/N)? " answ
[ "$answ" = "y" ] && { ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --extra-vars '{"create_mirror":true}' --limit layer1,lxc${CUSTOMER}-mirror,kvm${CUSTOMER}-mirror,${CUSTOMER:1}-mirror --tags starts_layer2,bootstrap,prep_hosts,mirror --skip-tags upload_kvmimages create.yml || exit 1; }
echo
echo "###########################################"
echo "# IV) create and bootstrap VMs & Containers"
echo "#                      (Layer 1 -> Layer 2)"
echo "###########################################"
echo         "command: ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags starts_layer2,bootstrap create.yml"
echo
read -p "Do you want to create and bootstrap the VMs & Containers (y/N)? " answ
[ "$answ" = "y" ] && { ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags starts_layer2,bootstrap create.yml || exit 1; }
echo
echo "###########################################"
echo "# V) prep Layer 2 (BM starts here)"
echo "###########################################"
echo         "command: ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags prep_hosts create.yml"
echo
read -p "Do you want to prepare the machines on Layer 2 (y/N)? " answ
[ "$answ" = "y" ] && { ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags prep_hosts create.yml || exit 1; }
echo
echo "###########################################"
echo "# VI) create Layer 2"
echo "###########################################"
echo          "command: ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags layer2 --skip-tags bootstrap,mirror create.yml"
echo
read -p "Do you want to create all other machines on Layer 2 (Y/n)? " answ
[ "$answ" != "n" ] && { ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags layer2 --skip-tags bootstrap,mirror create.yml || exit 1; }
echo
echo "###########################################"
echo "# VII) create Layer 3"
echo "###########################################"
echo       "command: ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags layer3 --skip-tags mirror create.yml"
echo
read -p "Do you want to create all other machines on Layer 3 (y/N)? " answ
[ "$answ" = "y" ] && ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml ${CONFIG} --tags layer3 --skip-tags mirror create.yml
